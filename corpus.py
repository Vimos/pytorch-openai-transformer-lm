import json
import os
import random
import re
import string
from collections import namedtuple, Counter, OrderedDict
from abc import ABC, abstractmethod

import ftfy
import numpy as np
import spacy
import torch
from Levenshtein import jaro_winkler, ratio
from scipy.optimize import linear_sum_assignment


def text_standardize(text):
    """
    fixes some issues the spacy tokenizer had on books corpus
    also does some whitespace standardization
    """
    text = text.replace('—', '-')
    text = text.replace('–', '-')
    text = text.replace('―', '-')
    text = text.replace('…', '...')
    text = text.replace('´', "'")
    text = re.sub(r'''(-+|~+|!+|"+|;+|\?+|\++|,+|\)+|\(+|\\+|\/+|\*+|\[+|\]+|}+|{+|\|+|_+)''', r' \1 ', text)
    text = re.sub(r'\s*\n\s*', ' \n ', text)
    text = re.sub(r'[^\S\n]+', ' ', text)
    return text.strip()


def reject_outliers(data, m=2.):
    d = np.abs(data - np.median(data))
    mdev = np.median(d)
    s = d / mdev if mdev else 0.
    return data[s < m]


def setjaro(set1, set2, center, lambda1=0.01):
    l1, l2 = len(set1), len(set2)

    C = np.zeros((l1, l2))
    for i, v in enumerate(set1):
        for j, w in enumerate(set2):
            # C[i, j] = jaro_winkler(v, w) - lambda1 * abs(i - center)
            C[i, j] = ratio(v, w) - lambda1 * abs(i - center)

    row_ind, col_ind = linear_sum_assignment(-C)
    return row_ind, col_ind


def positional_setjaro(set1, set2, left, right, lambda1=0.01):
    def position_cost(l, r, i, j):
        # return lambda1 * abs(i - c) + lambda1 * abs(i - j - row_ind_new.min())
        return lambda1 * (abs((l + r) / 2 - i) + 2 * abs(i - l - j))

    l1, l2 = len(set1), len(set2)
    C = np.zeros((l1, l2))
    for i, v in enumerate(set1):
        for j, w in enumerate(set2):
            C[i, j] = jaro_winkler(v, w, lambda1) - position_cost(left, right, i, j)
            # C[i, j] = ratio(v, w) - position_cost(left, i, j)

    row_ind, col_ind = linear_sum_assignment(-C)
    return row_ind, col_ind


def strictly_increasing(L):
    return all(x < y for x, y in zip(L, L[1:]))


def strictly_decreasing(L):
    return all(x > y for x, y in zip(L, L[1:]))


def non_increasing(L):
    return all(x >= y for x, y in zip(L, L[1:]))


def non_decreasing(L):
    return all(x <= y for x, y in zip(L, L[1:]))


class TextEncoder(ABC):
    """
    mostly a wrapper for a public python bpe tokenizer
    """

    def __init__(self, encoder_path, bpe_path):
        self.nlp = spacy.load('en', disable=['parser', 'tagger', 'ner', 'textcat'])
        self.encoder = json.load(open(encoder_path))
        self.decoder = {v: k for k, v in self.encoder.items()}
        merges = open(bpe_path, encoding='utf-8').read().split('\n')[1:-1]
        merges = [tuple(merge.split()) for merge in merges]
        self.bpe_ranks = dict(zip(merges, range(len(merges))))
        self.cache = {}
        self.parse_cache = {}
        self.count = {
            'positive': 0,
            'negative': 0,
        }

        self.special = ['_start_', '_delimiter_', '_classify_', '_end_']
        for t in self.special:
            self.encoder[t] = len(self.encoder)

    @property
    def start_token(self):
        return self.encoder['_start_']

    @property
    def end_token(self):
        return self.encoder['_end_']

    @property
    def delimiter_token(self):
        return self.encoder['_delimiter_']

    @property
    def clf_token(self):
        return self.encoder['_classify_']

    @property
    def n_vocab(self):
        return len(self.encoder)

    @staticmethod
    def get_pairs(word):
        """
        Return set of symbol pairs in a word.
        word is represented as tuple of symbols (symbols being variable-length strings)
        """
        pairs = set()
        prev_char = word[0]
        for char in word[1:]:
            pairs.add((prev_char, char))
            prev_char = char
        return pairs

    def bpe(self, token):
        word = tuple(token[:-1]) + (token[-1] + '</w>',)
        if token in self.cache:
            return self.cache[token]
        pairs = self.get_pairs(word)

        if not pairs:
            return token + '</w>'

        while True:
            bigram = min(pairs, key=lambda pair: self.bpe_ranks.get(pair, float('inf')))
            if bigram not in self.bpe_ranks:
                break
            first, second = bigram
            new_word = []
            i = 0
            while i < len(word):
                try:
                    j = word.index(first, i)
                    new_word.extend(word[i:j])
                    i = j
                except:
                    new_word.extend(word[i:])
                    break

                if word[i] == first and i < len(word) - 1 and word[i + 1] == second:
                    new_word.append(first + second)
                    i += 2
                else:
                    new_word.append(word[i])
                    i += 1
            new_word = tuple(new_word)
            word = new_word
            if len(word) == 1:
                break
            else:
                pairs = self.get_pairs(word)
        word = ' '.join(word)
        if word == '\n  </w>':
            word = '\n</w>'
        self.cache[token] = word
        return word

    @staticmethod
    def convert_idx(text, tokens):
        current = 0
        spans = []
        for token in tokens:
            current = text.find(token, current)
            if current < 0:
                print("Token {} cannot be found".format(token))
                raise Exception()
            spans.append((current, current + len(token)))
            current += len(token)
        return spans

    @staticmethod
    def remove_stopwords(words_list):

        stopwords = {'a': True,
                     'about': True,
                     'above': True,
                     'after': True,
                     'again': True,
                     'against': True,
                     'all': True,
                     'am': True,
                     'an': True,
                     'and': True,
                     'any': True,
                     'are': True,
                     'as': True,
                     'at': True,
                     'be': True,
                     'because': True,
                     'been': True,
                     'before': True,
                     'being': True,
                     'below': True,
                     'between': True,
                     'both': True,
                     'but': True,
                     'by': True,
                     'could': True,
                     'did': True,
                     'do': True,
                     'does': True,
                     'doing': True,
                     'down': True,
                     'during': True,
                     'each': True,
                     'few': True,
                     'for': True,
                     'from': True,
                     'further': True,
                     'had': True,
                     'has': True,
                     'have': True,
                     'having': True,
                     'he': True,
                     "he'd": True,
                     "he'll": True,
                     "he's": True,
                     'her': True,
                     'here': True,
                     "here's": True,
                     'hers': True,
                     'herself': True,
                     'him': True,
                     'himself': True,
                     'his': True,
                     'how': True,
                     "how's": True,
                     'i': True,
                     "i'd": True,
                     "i'll": True,
                     "i'm": True,
                     "i've": True,
                     'if': True,
                     'in': True,
                     'into': True,
                     'is': True,
                     'it': True,
                     "it's": True,
                     'its': True,
                     'itself': True,
                     "let's": True,
                     'me': True,
                     'more': True,
                     'most': True,
                     'my': True,
                     'myself': True,
                     'nor': True,
                     'of': True,
                     'on': True,
                     'once': True,
                     'only': True,
                     'or': True,
                     'other': True,
                     'ought': True,
                     'our': True,
                     'ours': True,
                     'ourselves': True,
                     'out': True,
                     'over': True,
                     'own': True,
                     'same': True,
                     'she': True,
                     "she'd": True,
                     "she'll": True,
                     "she's": True,
                     'should': True,
                     'so': True,
                     'some': True,
                     'such': True,
                     'than': True,
                     'that': True,
                     "that's": True,
                     'the': True,
                     'their': True,
                     'theirs': True,
                     'them': True,
                     'themselves': True,
                     'then': True,
                     'there': True,
                     "there's": True,
                     'these': True,
                     'they': True,
                     "they'd": True,
                     "they'll": True,
                     "they're": True,
                     "they've": True,
                     'this': True,
                     'those': True,
                     'through': True,
                     'to': True,
                     'too': True,
                     'under': True,
                     'until': True,
                     'up': True,
                     'very': True,
                     'was': True,
                     'we': True,
                     "we'd": True,
                     "we'll": True,
                     "we're": True,
                     "we've": True,
                     'were': True,
                     'what': True,
                     "what's": True,
                     'when': True,
                     "when's": True,
                     'where': True,
                     "where's": True,
                     'which': True,
                     'while': True,
                     'who': True,
                     "who's": True,
                     'whom': True,
                     'why': True,
                     "why's": True,
                     'with': True,
                     'would': True,
                     'you': True,
                     "you'd": True,
                     "you'll": True,
                     "you're": True,
                     "you've": True,
                     'your': True,
                     'yours': True,
                     'yourself': True,
                     'yourselves': True,
                     ',': True,
                     '"': True
                     }

        return list(filter(lambda x: not stopwords.get(x), words_list))

    def encode_text(self, text):
        text = self.nlp(text_standardize(ftfy.fix_text(text)))
        text_tokens = []
        for token in text:
            text_tokens.extend([self.encoder.get(t, 0) for t in self.bpe(token.text.lower()).split(' ')])
        return text_tokens

    def find_span_center(self, article, text_list, char_span, answer_list):
        spans = self.convert_idx(article, text_list)
        answer_span = []
        for idx, span in enumerate(spans):
            if not (char_span[1] <= span[0] or char_span[0] >= span[1]):
                answer_span.append(idx)
        span_center = np.median(answer_span)

        if np.isnan(span_center):
            span_center = sum(char_span) / 2 / len(article) * len(text_list)
        else:
            a_len = max(10, len(answer_list))
            left = max(0, int(span_center) - a_len)
            right = int(span_center) + a_len
            window = text_list[left:right]
            single = [window.index(w) for w in answer_list if window.count(w) == 1]
            if single:
                span_center = np.median(single) + left
        return span_center

    def encode_text_with_span(self, text, span_text, char_span):
        if text in self.parse_cache:
            article, text_list = self.parse_cache[text]
        else:
            article = text_standardize(ftfy.fix_text(text.replace('\n', '')))
            text_list = [t.text for t in self.nlp(article)]
            self.parse_cache[text] = (article, text_list)

        text_tokens = []
        token_list = []
        for i, token in enumerate(text_list):
            bpe_list = self.bpe(token.lower()).split(' ')
            text_tokens.extend([self.encoder.get(t, 0) for t in bpe_list])
            token_list.append(bpe_list)

        if span_text in ['unknown', ',']:
            return text_tokens, (-1, -1)

        answer_list = [t.text for t in
                       self.nlp(
                           text_standardize(ftfy.fix_text(
                               span_text.replace('\n', '').replace(',', ' ').replace('"', ' ').strip('.'))))]

        span_center = self.find_span_center(article, text_list, char_span, answer_list)
        row_ind_origin, col_ind_origin = setjaro(text_list,
                                                 answer_list,
                                                 span_center,
                                                 lambda1=0.001)
        try:
            left, right = int(min(row_ind_origin)), int(max(row_ind_origin))
        except Exception as e:
            print(e, "answer_list = ", answer_list)
            return text_tokens, (-1, -1)

        span = text_list[left:right + 1]
        if answer_list != list(filter(lambda x: x not in [',', '"'], span)):
            row_ind_new = reject_outliers(row_ind_origin, m=2)
            if len(answer_list) <= 2:
                row_ind, col_ind = positional_setjaro(text_list,
                                                      answer_list,
                                                      np.min(row_ind_new),
                                                      np.max(row_ind_new),
                                                      lambda1=0.0001)
                left, right = int(min(row_ind)), int(max(row_ind))
                span = text_list[left:right + 1]
                if (answer_list != list(filter(lambda x: x not in [',', '"'], span))) and ''.join(
                        answer_list) not in ''.join(span):
                    print('-------------------------')
                    print(span_center, row_ind_new, row_ind, col_ind)
                    print("answer_list = ", answer_list)
                    print("span = ", span)
                    print("new span = ", span)
                    self.count['negative'] += 1
                    print(self.count)
                else:
                    self.count['positive'] += 1
            else:
                row_ind, col_ind = positional_setjaro(text_list,
                                                      answer_list[1:-1],
                                                      np.min(row_ind_new),
                                                      np.max(row_ind_new),
                                                      lambda1=0.0001)
                # row_ind = reject_outliers(row_ind, m=2)
                left, right = int(min(row_ind)) - 1, int(max(row_ind)) + 1

                while text_list[left] in [',', '"']:
                    left -= 1

                while text_list[right] in [',', '"']:
                    right += 1

                span = text_list[left:right + 1]
                if answer_list[1:-1] != list(filter(lambda x: x not in [',', '"'], span))[1:-1]:
                    print('-------------------------')
                    print(span_center)
                    print(row_ind_origin, row_ind_new, row_ind, col_ind)
                    print(text_list)
                    print("answer_list = ", answer_list)
                    print("span = ", span)
                    print("new span = ", span)
                    self.count['negative'] += 1
                    print(self.count)
                else:
                    self.count['positive'] += 1
        else:
            self.count['positive'] += 1

        new_left, new_right = sum(map(len, token_list[:left])), sum(map(len, token_list[:right + 1]))
        # flat_token_list = [t for ts in token_list for t in ts]
        # print(span_text, self.bpe2str(flat_token_list[new_left:new_right]))
        return text_tokens, (new_left, new_right)

    def decode2bpe(self, token_list):
        return [self.decoder.get(k, '</w>') for k in token_list]

    def bpe2str(self, bpe_list):
        return ''.join(bpe_list).replace('</w>', ' ').strip()


class Corpus(TextEncoder):

    def __init__(self, corpus_path, encoder_path, bpe_path, split_names):
        super().__init__(encoder_path, bpe_path)
        self.corpus_path = corpus_path
        self.split_names = split_names
        self.data = {}
        self.data_encoded = {}

    def encode_path(self, split):
        return os.path.join(self.corpus_path, 'encode', "{}.p".format(split))

    @abstractmethod
    def encode_split(self, split_name):
        raise NotImplementedError

    def encode_data(self):
        data = {}
        for split_name in self.split_names:
            if os.path.isfile(self.encode_path(split_name)):
                data[split_name] = torch.load(self.encode_path(split_name))
            else:
                data[split_name] = [instance for instance in self.encode_split(split_name)]
                torch.save(data[split_name], self.encode_path(split_name))
        return data

    def data_len(self, data_type):
        return len(self.data_encoded[data_type])

    @abstractmethod
    def build_batch_record(self, data_list):
        raise NotImplementedError

    def batchify(self, data_type, batch_size, use_random=True):
        data_len = self.data_len(data_type)
        for i in range(data_len // batch_size):
            if use_random:
                data_list = [self.data_encoded[data_type][j] for j in random.sample(range(data_len), batch_size)]
            else:
                start = i * batch_size
                end = (i + 1) * batch_size
                data_list = self.data_encoded[data_type][start:end]
            br = self.build_batch_record(data_list)
            if br:
                yield br


class CoQA(Corpus):
    BatchRecord = namedtuple('BatchRecord', 'qids, answers, answers_masks, documents, masks, spans, spans_masks, labels')
    GroundTruth = namedtuple('GroundTruth', 'span_text, input_text')
    out_domain = ["reddit", "science"]
    in_domain = ["mctest", "gutenberg", "race", "cnn", "wikipedia"]
    domain_mappings = {"mctest": "children_stories",
                       "gutenberg": "literature",
                       "race": "mid-high_school",
                       "cnn": "news",
                       "wikipedia": "wikipedia",
                       "science": "science",
                       "reddit": "reddit"}

    def __init__(self, task_dir, encoder_path, bpe_path, n_ctx, debug=False):
        super().__init__(task_dir, encoder_path, bpe_path, split_names=['train', 'dev'])
        self.debug = debug
        self.n_ctx = n_ctx
        self.q_idx = {}
        self.ground_truths = {}
        self.id_to_source = {}
        self.data = self.load_data()
        self.data_encoded = self.encode_data()
        self.stats = {}

    def data_path(self, split):
        return os.path.join(self.corpus_path, 'CoQA', "coqa-{}-v1.0.json".format(split))

    def load_data(self):
        data = {}
        for split_name in self.split_names:
            count = 0
            qids, articles, questions, answers, char_spans = [], [], [], [], []
            print("Loading CoQA from {}".format(self.data_path(split_name)))
            with open(self.data_path(split_name), 'r', encoding='utf-8') as fpr:
                data_raw = json.load(fpr)
                for d in data_raw["data"]:
                    article = d['story']
                    article_id = d['id']
                    self.id_to_source[article_id] = d['source']
                    multiple_answers = [d['answers']]
                    if 'additional_answers' in d:
                        multiple_answers += d['additional_answers'].values()
                    conversation = ''
                    for question, answer in zip(d['questions'], d['answers']):
                        turn_id = question['turn_id']
                        assert turn_id == answer['turn_id']

                        qid = (article_id, turn_id)
                        self.ground_truths[qid] = [self.GroundTruth(span_text=a['span_text'],
                                                                    input_text=a['input_text']) for m in
                                                   zip(*multiple_answers) for
                                                   a in m if a['turn_id'] == turn_id]
                        self.q_idx[qid] = (split_name, len(qids))

                        qids.append(qid)
                        articles.append(article)
                        questions.append(conversation + question['input_text'])
                        char_spans.append([answer['span_start'], answer['span_end']])
                        answers.append(self.GroundTruth(span_text=answer['span_text'],
                                                        input_text=answer['input_text']))
                        if answer['span_text'] != 'unknown':
                            conversation += " {} {}.".format(question['input_text'], answer['input_text'])
                        count += 1
                    if self.debug and count > 100:
                        break
            data[split_name] = qids, articles, questions, answers, char_spans
        return data

    def encode_split(self, split_name):
        for qid, article, question, answer, char_span in zip(*self.data[split_name]):
            article_enc, bpe_span = self.encode_text_with_span(article, answer.span_text, char_span)
            question_enc = self.encode_text(question)
            answer_enc = self.encode_text(answer.input_text)
            span_enc = self.encode_text(answer.span_text)
            yield {
                "qid": qid,
                "article": article_enc,
                "question": question_enc,
                "span": span_enc,
                "answer": answer_enc,
                "bpe_span": bpe_span
            }

    def transform(self, data_list):
        n_batch = len(data_list)
        sent_num_max = 0

        qids = [a['qid'] for a in data_list]
        article_len = [len(a['article']) for a in data_list]
        question_len = [len(a['question']) for a in data_list]

        len_info = []
        for al, ql in zip(article_len, question_len):
            sent_num_max = max(sent_num_max, al + ql + 3)
            if al + ql + 3 <= self.n_ctx:
                len_info.append((al, ql))
            else:
                ql = min(ql, 100)
                al = self.n_ctx - 3 - ql
                len_info.append((al, ql))

        # sent_num_max = min(self.n_ctx, sent_num_max)
        sent_num_max = self.n_ctx

        x = np.zeros((n_batch, 1, sent_num_max, 2))
        mx = np.zeros((n_batch, 1, sent_num_max))
        a = np.zeros((n_batch, 1, sent_num_max, 2))
        ma = np.zeros((n_batch, 1, sent_num_max))
        s = np.zeros((n_batch, 1, sent_num_max, 2))
        ms = np.zeros((n_batch, 1, sent_num_max))
        for i, (instance, (al, ql)) in enumerate(zip(data_list, len_info)):
            x_aqo = [self.start_token] + instance['article'][:int(al)] + [self.delimiter_token] + instance['question'][
                                                                                      -int(ql):] + [self.clf_token]
            l_aqo = len(x_aqo)
            x[i, 0, :l_aqo, 0] = x_aqo
            mx[i, 0, :l_aqo] = 1

            x_ans = [self.start_token] + instance['answer'] + [self.end_token]
            l_ans = len(x_ans)
            a[i, 0, :l_ans, 0] = x_ans
            ma[i, 0, :l_ans] = 1

            x_span = [self.start_token] + instance['span'] + [self.end_token]
            l_span = len(x_span)
            s[i, 0, :l_span, 0] = x_span
            ms[i, 0, :l_span] = 1
        # Position information that is added to the input embeddings in the TransformerModel
        x[:, :, :, 1] = np.arange(self.n_vocab, self.n_vocab + sent_num_max)
        a[:, :, :, 1] = np.arange(self.n_vocab, self.n_vocab + sent_num_max)
        s[:, :, :, 1] = np.arange(self.n_vocab, self.n_vocab + sent_num_max)
        bpe_spans = np.array([a['bpe_span'] for a in data_list]) + 1
        return qids, a, ma, x, mx, s, ms, bpe_spans

    def filter_record(self, data_list):
        new_d = []
        for d in data_list:
            bpe_span = d['bpe_span']
            al = len(d['article'])
            ql = len(d['question'])
            if al + ql + 3 > self.n_ctx:
                ql = min(ql, 100)
                al = self.n_ctx - 3 - ql
                if bpe_span[1] > al:
                    print("Question {} is filtered".format(d['qid']))
                    continue
            new_d.append(d)
        return new_d

    def build_batch_record(self, data_list):
        data_list = self.filter_record(data_list)
        if len(data_list) == 0:
            return
        qids, answers, answers_masks, documents, masks, spans, spans_masks, bpe_spans = self.transform(data_list)

        batch_record = self.BatchRecord(qids=qids,
                                        answers=torch.Tensor(answers).long(),
                                        answers_masks=torch.Tensor(answers_masks),
                                        spans=torch.Tensor(spans).long(),
                                        spans_masks=torch.Tensor(spans_masks),
                                        documents=torch.Tensor(documents).long(),
                                        masks=torch.Tensor(masks),
                                        labels=torch.Tensor(bpe_spans).long())
        return batch_record

    @staticmethod
    def normalize_answer(s):
        """Lower text and remove punctuation, storys and extra whitespace."""

        def remove_articles(text):
            regex = re.compile(r'\b(a|an|the)\b', re.UNICODE)
            return re.sub(regex, ' ', text)

        def white_space_fix(text):
            return ' '.join(text.split())

        def remove_punc(text):
            exclude = set(string.punctuation)
            return ''.join(ch for ch in text if ch not in exclude)

        def lower(text):
            return text.lower()

        return white_space_fix(remove_articles(remove_punc(lower(s))))

    def get_tokens(self, s):
        if not s:
            return []
        return self.normalize_answer(s).split()

    def compute_exact(self, a_gold, a_pred):
        return int(self.normalize_answer(a_gold) == self.normalize_answer(a_pred))

    def compute_f1(self, a_gold, a_pred):
        gold_toks = self.get_tokens(a_gold)
        pred_toks = self.get_tokens(a_pred)
        common = Counter(gold_toks) & Counter(pred_toks)
        num_same = sum(common.values())
        if len(gold_toks) == 0 or len(pred_toks) == 0:
            # If either is no-answer, then F1 is 1 if they agree, 0 otherwise
            return int(gold_toks == pred_toks)
        if num_same == 0:
            return 0
        precision = 1.0 * num_same / len(pred_toks)
        recall = 1.0 * num_same / len(gold_toks)
        f1 = (2 * precision * recall) / (precision + recall)
        return f1

    def _compute_turn_score(self, a_gold_list, a_pred):
        f1_sum = 0.0
        em_sum = 0.0
        if len(a_gold_list) > 1:
            for i in range(len(a_gold_list)):
                # exclude the current answer
                gold_answers = a_gold_list[0:i] + a_gold_list[i + 1:]
                em_sum += max(self.compute_exact(a, a_pred) for a in gold_answers)
                f1_sum += max(self.compute_f1(a, a_pred) for a in gold_answers)
        else:
            em_sum += max(self.compute_exact(a, a_pred) for a in a_gold_list)
            f1_sum += max(self.compute_f1(a, a_pred) for a in a_gold_list)

        return {'em': em_sum / max(1, len(a_gold_list)), 'f1': f1_sum / max(1, len(a_gold_list))}

    def compute_turn_score(self, story_id, turn_id, a_pred):
        """
        This is the function what you are probably looking for. a_pred is the answer string your model predicted.
        """
        key = (story_id, turn_id)
        a_gold_list = self.ground_truths[key]
        return self._compute_turn_score(a_gold_list, a_pred)

    def get_domain_scores(self, exact_scores, f1_scores):
        sources = {}
        for source in self.in_domain + self.out_domain:
            sources[source] = Counter()

        for story_id, turn_id in self.ground_truths:
            key = (story_id, turn_id)
            source = self.id_to_source[story_id]
            sources[source]['em_total'] += exact_scores.get(key, 0)
            sources[source]['f1_total'] += f1_scores.get(key, 0)
            sources[source]['turn_count'] += 1

        scores = OrderedDict()
        in_domain_em_total = 0.0
        in_domain_f1_total = 0.0
        in_domain_turn_count = 0

        out_domain_em_total = 0.0
        out_domain_f1_total = 0.0
        out_domain_turn_count = 0

        for source in self.in_domain + self.out_domain:
            domain = self.domain_mappings[source]
            scores[domain] = {}
            scores[domain]['em'] = round(sources[source]['em_total'] / max(1, sources[source]['turn_count']) * 100, 1)
            scores[domain]['f1'] = round(sources[source]['f1_total'] / max(1, sources[source]['turn_count']) * 100, 1)
            scores[domain]['turns'] = sources[source]['turn_count']
            if source in self.in_domain:
                in_domain_em_total += sources[source]['em_total']
                in_domain_f1_total += sources[source]['f1_total']
                in_domain_turn_count += sources[source]['turn_count']
            elif source in self.out_domain:
                out_domain_em_total += sources[source]['em_total']
                out_domain_f1_total += sources[source]['f1_total']
                out_domain_turn_count += sources[source]['turn_count']

        scores["in_domain"] = {'em': round(in_domain_em_total / max(1, in_domain_turn_count) * 100, 1),
                               'f1': round(in_domain_f1_total / max(1, in_domain_turn_count) * 100, 1),
                               'turns': in_domain_turn_count}
        scores["out_domain"] = {'em': round(out_domain_em_total / max(1, out_domain_turn_count) * 100, 1),
                                'f1': round(out_domain_f1_total / max(1, out_domain_turn_count) * 100, 1),
                                'turns': out_domain_turn_count}

        em_total = in_domain_em_total + out_domain_em_total
        f1_total = in_domain_f1_total + out_domain_f1_total
        turn_count = in_domain_turn_count + out_domain_turn_count
        scores["overall"] = {'em': round(em_total / max(1, turn_count) * 100, 1),
                             'f1': round(f1_total / max(1, turn_count) * 100, 1),
                             'turns': turn_count}

        return scores

    def evaluate(self, qids, bpe_spans):
        f1 = exact_match = total = 0
        for qid, bpe_span in zip(qids, bpe_spans):
            split, idx = self.q_idx[qid]
            document = self.data_encoded[split][idx]['article']
            bpe_list = self.decode2bpe(document[bpe_span[0] - 1:bpe_span[1] - 1])
            if bpe_list:
                span_txt = self.bpe2str(bpe_list)
            else:
                span_txt = ''
            total += 1
            ground_truths = [a.span_text for a in self.ground_truths[qid]]
            prediction = span_txt
            scores = self._compute_turn_score(ground_truths, prediction)
            # Take max over all gold answers
            exact_match += scores['em']
            f1 += scores['f1']
        exact_match = 100.0 * exact_match / total
        f1 = 100.0 * f1 / total
        return {'exact_match': exact_match, 'f1': f1}

    def evaluate_generation(self, qids, gen_tokens):
        f1 = exact_match = total = 0
        for qid, gen_token in zip(qids, gen_tokens):
            bpe_list = self.decode2bpe(gen_token.cpu().numpy())
            if bpe_list:
                span_txt = self.bpe2str(bpe_list)
            else:
                span_txt = ''
            total += 1
            ground_truths = [a.input_text for a in self.ground_truths[qid]]
            prediction = span_txt
            if prediction.strip():
                print(qid, ground_truths, prediction)
            scores = self._compute_turn_score(ground_truths, prediction)
            # Take max over all gold answers
            exact_match += scores['em']
            f1 += scores['f1']
        exact_match = 100.0 * exact_match / total
        f1 = 100.0 * f1 / total
        return {'exact_match': exact_match, 'f1': f1}


class SQuAD(Corpus):
    BatchRecord = namedtuple('BatchRecord', 'qids, documents, masks, labels')

    def __init__(self, task_dir, encoder_path, bpe_path, n_ctx, debug=False):
        super().__init__(task_dir, encoder_path, bpe_path, ['train', 'dev'])
        self.debug = debug
        self.n_ctx = n_ctx
        self.q_idx = {}
        self.ground_truths = {}
        self.data = self.load_data()
        self.data_encoded = self.encode_data()
        self.stats = {}

    def data_path(self, dataset_name):
        return os.path.join(self.corpus_path, "SQuAD", "{}-v1.1.json".format(dataset_name))

    def load_data(self):
        data = {}
        for split_name in self.split_names:
            source_data = json.load(open(self.data_path(split_name), 'r', encoding='utf8'))

            count = 0

            qids, articles, questions, answers, char_spans = [], [], [], [], []
            for ai, article in enumerate(source_data['data']):
                for pi, para in enumerate(article['paragraphs']):
                    context = para['context']
                    for qa in para['qas']:
                        qid = qa['id']
                        question = qa['question']
                        answer = qa['answers'][0]
                        answer_text = answer['text']
                        answer_start = answer['answer_start']
                        answer_stop = answer_start + len(answer_text)

                        self.ground_truths[qid] = [a['text'] for a in qa['answers']]
                        self.q_idx[qid] = (split_name, len(qids))

                        qids.append(qid)
                        articles.append(context)
                        questions.append(question)
                        answers.append(answer_text)
                        char_spans.append([answer_start, answer_stop])
                        count += 1
                if self.debug and count > 10:
                    break
            data[split_name] = qids, articles, questions, answers, char_spans
        return data

    def encode_split(self, split_name):
        for qid, article, question, answer, char_span in zip(*self.data[split_name]):
            article_enc, bpe_span = self.encode_text_with_span(article, answer, char_span)
            question_enc = self.encode_text(question)
            yield {
                "qid": qid,
                "article": article_enc,
                "question": question_enc,
                "bpe_span": bpe_span
            }

    def transform(self, data_list):
        n_batch = len(data_list)
        sent_num_max = 0

        qids = [a['qid'] for a in data_list]
        article_len = [len(a['article']) for a in data_list]
        question_len = [len(a['question']) for a in data_list]

        len_info = []
        for al, ql in zip(article_len, question_len):
            sent_num_max = max(sent_num_max, al + ql + 3)
            if al + ql + 3 <= self.n_ctx:
                len_info.append((al, ql))
            else:
                ql = min(ql, 100)
                al = self.n_ctx - 3 - ql
                len_info.append((al, ql))

        # sent_num_max = min(self.n_ctx, sent_num_max)
        sent_num_max = self.n_ctx

        xmb = np.zeros((n_batch, 1, sent_num_max, 2))
        mmb = np.zeros((n_batch, 1, sent_num_max))
        for i, (instance, (al, ql)) in enumerate(zip(data_list, len_info)):
            x_aqo = [self.start_token] + instance['article'][:int(al)] + [self.delimiter_token] + instance['question'][
                                                                                      -int(ql):] + [self.clf_token]
            l_aqo = len(x_aqo)
            xmb[i, 0, :l_aqo, 0] = x_aqo
            mmb[i, 0, :l_aqo] = 1
        # Position information that is added to the input embeddings in the TransformerModel
        xmb[:, :, :, 1] = np.arange(self.n_vocab, self.n_vocab + sent_num_max)
        bpe_spans = np.array([a['bpe_span'] for a in data_list]) + 1
        return qids, xmb, mmb, bpe_spans

    def filter_record(self, data_list):
        new_d = []
        for d in data_list:
            bpe_span = d['bpe_span']
            al = len(d['article'])
            ql = len(d['question'])
            if al + ql + 3 > self.n_ctx:
                ql = min(ql, 100)
                al = self.n_ctx - 3 - ql
                if bpe_span[1] > al:
                    print("Question {} is filtered".format(d['qid']))
                    continue
            new_d.append(d)
        return new_d

    def build_batch_record(self, data_list):
        data_list = self.filter_record(data_list)
        if len(data_list) == 0:
            return
        qids, documents, masks, bpe_spans = self.transform(data_list)

        batch_record = self.BatchRecord(qids=qids,
                                        documents=torch.Tensor(documents).long(),
                                        masks=torch.Tensor(masks),
                                        labels=torch.Tensor(bpe_spans).long())
        return batch_record

    @staticmethod
    def convert_tokens(eval_file, qa_id, pp1, pp2):
        answer_dict = {}
        remapped_dict = {}
        for qid, p1, p2 in zip(qa_id, pp1, pp2):
            context = eval_file[str(qid)]["context"]
            spans = eval_file[str(qid)]["spans"]
            uuid = eval_file[str(qid)]["uuid"]
            start_idx = spans[p1][0]
            end_idx = spans[p2][1]
            answer_dict[str(qid)] = context[start_idx: end_idx]
            remapped_dict[uuid] = context[start_idx: end_idx]
        return answer_dict, remapped_dict

    def evaluate_file(self, eval_file, answer_dict):
        f1 = exact_match = total = 0
        for key, value in answer_dict.items():
            total += 1
            ground_truths = eval_file[key]["answers"]
            prediction = value
            exact_match += self.metric_max_over_ground_truths(
                self.exact_match_score, prediction, ground_truths)
            f1 += self.metric_max_over_ground_truths(self.f1_score,
                                                     prediction, ground_truths)
        exact_match = 100.0 * exact_match / total
        f1 = 100.0 * f1 / total
        return {'exact_match': exact_match, 'f1': f1}

    def evaluate(self, qids, bpe_spans):
        f1 = exact_match = total = 0
        for qid, bpe_span in zip(qids, bpe_spans):
            split, idx = self.q_idx[qid]
            document = self.data_encoded[split][idx]['article']
            bpe_list = self.decode2bpe(document[bpe_span[0] - 1:bpe_span[1] - 1])
            if bpe_list:
                span_txt = self.bpe2str(bpe_list)
            else:
                span_txt = ''
            total += 1
            ground_truths = self.ground_truths[qid]
            prediction = span_txt
            exact_match += self.metric_max_over_ground_truths(
                self.exact_match_score, prediction, ground_truths)
            f1 += self.metric_max_over_ground_truths(self.f1_score,
                                                     prediction, ground_truths)
        exact_match = 100.0 * exact_match / total
        f1 = 100.0 * f1 / total
        return {'exact_match': exact_match, 'f1': f1}

    @staticmethod
    def normalize_answer(s):
        def remove_articles(text):
            return re.sub(r'\b(a|an|the)\b', ' ', text)

        def white_space_fix(text):
            return ' '.join(text.split())

        def remove_punc(text):
            exclude = set(string.punctuation)
            return ''.join(ch for ch in text if ch not in exclude)

        def lower(text):
            return text.lower()

        return white_space_fix(remove_articles(remove_punc(lower(s))))

    def f1_score(self, prediction, ground_truth):
        prediction_tokens = self.normalize_answer(prediction).split()
        ground_truth_tokens = self.normalize_answer(ground_truth).split()
        common = Counter(prediction_tokens) & Counter(ground_truth_tokens)
        num_same = sum(common.values())
        if num_same == 0:
            return 0
        precision = 1.0 * num_same / len(prediction_tokens)
        recall = 1.0 * num_same / len(ground_truth_tokens)
        f1 = (2 * precision * recall) / (precision + recall)
        return f1

    def exact_match_score(self, prediction, ground_truth):
        return self.normalize_answer(prediction) == self.normalize_answer(ground_truth)

    @staticmethod
    def metric_max_over_ground_truths(metric_fn, prediction, ground_truths):
        scores_for_ground_truths = []
        for ground_truth in ground_truths:
            score = metric_fn(prediction, ground_truth)
            scores_for_ground_truths.append(score)
        return max(scores_for_ground_truths)


class CorpusReader(object):

    def __init__(self, task, data_dir, encoder_path, bpe_path, n_ctx, debug=False):
        self.task = task
        self.data_dir = data_dir
        if task == 'squad':
            self.corpus = SQuAD(self.corpus_dir, encoder_path, bpe_path, n_ctx, debug)
        elif task == 'coqa':
            self.corpus = CoQA(self.corpus_dir, encoder_path, bpe_path, n_ctx, debug)
        else:
            assert task in ['coqa', 'squad']

    @property
    def corpus_dir(self):
        return os.path.join(self.data_dir, self.task)


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('--data_dir', type=str, default='data/')
    parser.add_argument('--encoder_path', type=str, default='model/encoder_bpe_40000.json')
    parser.add_argument('--bpe_path', type=str, default='model/vocab_40000.bpe')
    parser.add_argument('--n_ctx', type=int, default=512)
    parser.add_argument('--debug', action='store_true')

    args = parser.parse_args()

    task = 'coqa'

    corpus_reader = CorpusReader(task,
                                 args.data_dir,
                                 args.encoder_path,
                                 args.bpe_path,
                                 args.n_ctx,
                                 debug=args.debug)
    for batch in corpus_reader.corpus.batchify('dev', 10):
        print(batch)
