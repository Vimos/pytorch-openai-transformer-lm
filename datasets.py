import csv
import json
import os
import re

import numpy as np
from sklearn.model_selection import train_test_split
from tqdm import tqdm

seed = 3535999445


def _rocstories(path):
    with open(path, encoding='utf_8') as f:
        f = csv.reader(f)
        st = []
        ct1 = []
        ct2 = []
        y = []
        for i, line in enumerate(tqdm(list(f), ncols=80, leave=False)):
            if i > 0:
                s = ' '.join(line[1:5])
                c1 = line[5]
                c2 = line[6]
                st.append(s)
                ct1.append(c1)
                ct2.append(c2)
                y.append(int(line[-1]) - 1)
        return st, ct1, ct2, y


def rocstories(data_dir, n_train=1497, n_valid=374):
    storys, comps1, comps2, ys = _rocstories(
        os.path.join(data_dir, 'cloze_test_val__spring2016 - cloze_test_ALL_val.csv'))
    teX1, teX2, teX3, _ = _rocstories(os.path.join(data_dir, 'cloze_test_test__spring2016 - cloze_test_ALL_test.csv'))
    tr_storys, va_storys, tr_comps1, va_comps1, tr_comps2, va_comps2, tr_ys, va_ys = train_test_split(storys, comps1,
                                                                                                      comps2, ys,
                                                                                                      test_size=n_valid,
                                                                                                      random_state=seed)
    trX1, trX2, trX3 = [], [], []
    trY = []
    for s, c1, c2, y in zip(tr_storys, tr_comps1, tr_comps2, tr_ys):
        trX1.append(s)
        trX2.append(c1)
        trX3.append(c2)
        trY.append(y)

    vaX1, vaX2, vaX3 = [], [], []
    vaY = []
    for s, c1, c2, y in zip(va_storys, va_comps1, va_comps2, va_ys):
        vaX1.append(s)
        vaX2.append(c1)
        vaX3.append(c2)
        vaY.append(y)
    trY = np.asarray(trY, dtype=np.int32)
    vaY = np.asarray(vaY, dtype=np.int32)
    return {'train': (trX1, trX2, trX3, trY),
            'dev': (vaX1, vaX2, vaX3, vaY),
            'test': (teX1, teX2, teX3)}


def multirc(multirc_dir, multi=False):
    dataset_names = ['train', 'dev']
    json_dir = os.path.join(multirc_dir, 'multirc-v2', 'splitv2')
    dev_json = os.path.join(json_dir, 'dev_83-fixedIds.json')
    train_json = os.path.join(json_dir, 'train_456-fixedIds.json')
    text_re = re.compile('<b>Sent \d+: </b>(.*?)<br>')
    q_id = 0
    data = {}
    for dataset_name, data_json in zip(dataset_names, [train_json, dev_json]):
        articles, questions, options, labels = [], [], [], []
        id_map = {}
        with open(data_json, "r", encoding='utf-8') as fh:
            data_raw = json.load(fh)
            for para in data_raw['data']:
                sentences = [s.strip() for s in
                             text_re.findall(para['paragraph']["text"])]
                article = ' '.join(sentences)

                for idx, qa in enumerate(para['paragraph']['questions']):
                    question = qa["question"]
                    if multi:
                        option = [answer['text'] for answer in qa['answers']]
                        ground_truth = [int(answer['isAnswer']) for answer in qa['answers']]

                        articles.append(article)
                        questions.append(question)
                        options.append(option)
                        labels.append(ground_truth)

                        id_map[q_id] = {
                            'pid': para['id'],
                            'qid': str(idx)
                        }
                        q_id += 1
                        if q_id % 1000 == 0:
                            print(q_id)
                    else:
                        for j, answer in enumerate(qa['answers']):
                            option = answer['text']
                            ground_truth = int(answer['isAnswer'])

                            articles.append(article)
                            questions.append(question)
                            options.append(option)
                            labels.append(ground_truth)

                            id_map[q_id] = {
                                'pid': para['id'],
                                'qid': str(idx),
                                'oid': j,
                                'label': ground_truth,
                                'label_len': len(qa['answers'])
                            }
                            q_id += 1
                            if q_id % 1000 == 0:
                                print(q_id)
        if multi:
            option_len = max(list(map(len, options)))
            np_labels = np.zeros((len(options), option_len))
            for i, label in enumerate(labels):
                for j, v in enumerate(label):
                    np_labels[i, j] = v
        else:
            np_labels = np.asarray(labels, dtype=np.int32)
        data[dataset_name] = (articles, questions, options, np_labels)

        with open(os.path.join(multirc_dir, '{}_map.json'.format(dataset_name)), 'w',
                  encoding='utf-8') as fpw:
            json.dump(id_map, fpw, indent=2)
    data['test'] = data['dev']
    return data


def race(race_dir):
    dataset_names = ['train', 'dev', 'test']
    level_names = ['high', 'middle']
    label_dict = {'A': 0, 'B': 1, 'C': 2, 'D': 3}

    data = {}
    for dataset_name in dataset_names:
        articles, questions, options, labels = [], [], [], []
        for level_name in level_names:
            path = os.path.join(race_dir, 'RACE', dataset_name, level_name)
            for filename in os.scandir(path):
                with open(filename.path, 'r', encoding='utf-8') as fpr:
                    data_raw = json.load(fpr)
                    article = data_raw['article']
                    for question, option, answer in zip(data_raw['questions'], data_raw['options'],
                                                        data_raw['answers']):
                        articles.append(article)
                        questions.append(question)
                        options.append(option)
                        labels.append(label_dict[answer])
        labels = np.asarray(labels, dtype=np.int32)
        data[dataset_name] = (articles, questions, options, labels)
    # vaY = np.asarray(vaY, dtype=np.int32)
    return data


def coqa(coqa_dir, debug=False):
    dataset_names = ['train', 'dev']

    data = {}
    for dataset_name in dataset_names:
        count = 0
        articles, questions, answers, labels = [], [], [], []
        path = os.path.join(coqa_dir, 'CoQA', "coqa-{}-v1.0.json".format(dataset_name))
        print("Loading CoQA from {}".format(path))
        with open(path, 'r', encoding='utf-8') as fpr:
            data_raw = json.load(fpr)
            for d in data_raw["data"]:
                article = d['story']
                conversation = ''
                for question, answer in zip(d['questions'], d['answers']):
                    articles.append(article)
                    questions.append(conversation + question['input_text'])
                    labels.append([answer['span_start'], answer['span_end']])
                    answers.append(answer['span_text'])
                    conversation += " {} {}.".format(question['input_text'], answer['input_text'])
                    count += 1
                if debug and count > 100:
                    break
        labels = np.asarray(labels, dtype=np.int32)
        data[dataset_name] = (articles, questions, answers, labels)
    # vaY = np.asarray(vaY, dtype=np.int32)
    data['test'] = data['dev']
    return data


def squad(squad_dir, debug=False):
    dataset_names = ['train', 'dev']
    data = {}
    for dataset_name in dataset_names:
        source_path = os.path.join(squad_dir, "SQuAD", "{}-v1.1.json".format(dataset_name))
        source_data = json.load(open(source_path, 'r', encoding='utf8'))

        count = 0

        articles, questions, answers, labels = [], [], [], []
        for ai, article in enumerate(source_data['data']):
            for pi, para in enumerate(article['paragraphs']):
                context = para['context']
                for qa in para['qas']:
                    question = qa['question']
                    answer = qa['answers'][0]
                    answer_text = answer['text']
                    answer_start = answer['answer_start']
                    answer_stop = answer_start + len(answer_text)

                    articles.append(context)
                    questions.append(question)
                    answers.append(answer_text)
                    labels.append([answer_start, answer_stop])
                    count += 1
            if debug and count > 10:
                break
        labels = np.asarray(labels, dtype=np.int32)
        data[dataset_name] = (articles, questions, answers, labels)
    data['test'] = data['dev']
    return data
