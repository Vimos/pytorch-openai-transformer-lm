import argparse
import os
import random

import numpy as np
import torch
import torch.nn as nn
from sklearn.metrics import accuracy_score
from tensorboardX import SummaryWriter
from tqdm import tqdm

from corpus import CorpusReader
from loss import MultipleChoiceLossCompute, GenerationLossCompute
from model import DoubleHeadModel, load_openai_pretrained_model, TripleHeadModel
from opt import OpenAIAdam
from utils import greedy_select, INF


class Logits(object):
    lm_logits = None
    clf_logits = None
    gen_logits = None

    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)


class Evaluator(object):

    def __init__(self, corpus_reader, save_path, summary_writer, batch_train, generate=False):
        self.corpus_reader = corpus_reader
        self.save_path = save_path
        self.summary_writer = summary_writer
        self.generate = generate
        self.n_batch_eval = batch_train * 2

    @property
    def description(self):
        return "Eval {} Epoch {} Step {}"

    @staticmethod
    def inference(model, x, s=None, max_a_len=None):
        if s is not None:
            d_logits = model(x, s, max_a_len)
        else:
            d_logits = model(x)
        logits = Logits(lm_logits=d_logits['lm_logits'],
                        clf_logits=d_logits['clf_logits'],
                        gen_logits=d_logits.get('gen_logits'))
        logits.clf_logits = logits.clf_logits.view((-1, logits.clf_logits.size(-1)))
        logits.lm_logits = None
        return logits

    def iterate(self, model, epoch, iteration, split):
        for j, br in tqdm(enumerate(self.corpus_reader.corpus.batchify('dev',
                                                                       batch_size=self.n_batch_eval,
                                                                       use_random=False)),
                          ncols=80,
                          desc=self.description.format(split, epoch, iteration),
                          total=self.corpus_reader.corpus.data_len('dev') // self.n_batch_eval):
            X = br.documents.to(device)
            MX = br.masks.to(device)

            Y = br.labels.to(device)
            if generate:
                A = br.answers.to(device)
                MA = br.answers_masks.to(device)
                S = br.spans.to(device)
                MS = br.spans_masks.to(device)
                max_a_len = MS.long().sum(-1).max().item()

                logits = self.inference(model, X, S, max_a_len)

                clf_losses = compute_loss_fct(X, Y.view(-1, ), MX, (A, MA),
                                              logits=logits, only_return_losses=True, split=split,
                                              iteration=iteration + j)
                yield (br.qids, logits.clf_logits, logits.gen_logits.argmax(-1), Y, clf_losses.sum().item())
            else:
                logits = self.inference(model, X)
                clf_losses = compute_loss_fct(X, Y.view(-1, ), MX,
                                              logits=logits, only_return_losses=True, split=split,
                                              iteration=iteration + j)
                yield (br.qids, logits.clf_logits, None, Y, clf_losses.sum().item())

    @staticmethod
    def optimize_span(span_logits):
        left_logits, right_logits = span_logits[:, 0, :], span_logits[:, 1, :]
        left_span = greedy_select(masked_logits=left_logits)
        left_span_mask = (1 - left_span.cumsum(1))
        new_right_logits = (left_span + left_span_mask).float() * (-INF) + right_logits
        new_span_logits = torch.cat([left_logits.unsqueeze(1), new_right_logits.unsqueeze(1)], dim=1)
        return torch.argmax(new_span_logits, 2)

    def evaluate(self, model, epoch, iteration, split):
        with torch.no_grad():
            model.eval()
            qids_list = []
            logits_list = []
            labels_list = []
            gen_list = []
            cost = 0
            count = 0

            for qids, clf_logits, gen_tokens, lables, clf_loss in self.iterate(model, epoch, iteration, split):
                qids_list.extend(qids)
                logits_list.append(clf_logits)
                gen_list.append(gen_tokens)
                labels_list.append(lables)
                cost += clf_loss
                count += len(qids)
                # if count > 5:
                #     break

            logits_t = torch.cat(logits_list, 0).view(-1, 2, logits_list[0].size(-1))
            labels_t = torch.cat(labels_list, 0)

            y = labels_t
            y_pred = torch.argmax(logits_t, 2)

            acc = accuracy_score(y.view(-1).cpu().numpy(), y_pred.view(-1).cpu().numpy())
            self.summary_writer.add_scalar('{}/cost'.format(split), cost / count, iteration)
            self.summary_writer.add_scalar('{}/acc'.format(split), acc, iteration)
            tqdm.write("Accuracy for {} is {}".format(split, acc))

            metrics = self.corpus_reader.corpus.evaluate(qids_list, y_pred.cpu().numpy())
            for m in metrics:
                self.summary_writer.add_scalar('{}_span/{}'.format(split, m), metrics[m], iteration)

            metrics = self.corpus_reader.corpus.evaluate(qids_list, self.optimize_span(logits_t).cpu().numpy())
            for m in metrics:
                self.summary_writer.add_scalar('{}_span_optimized/{}'.format(split, m), metrics[m], iteration)

            if self.generate:
                # gen_t = torch.cat(gen_list, 0).view(-1, MA.size(-1))
                generate_metrics = self.corpus_reader.corpus.evaluate_generation(qids_list, gen_list)
                for m in generate_metrics:
                    self.summary_writer.add_scalar('{}_generation/{}'.format(split, m), generate_metrics[m], iteration)


def count_parameters(model):
    return sum(p.numel() for p in model.parameters() if p.requires_grad)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--desc', type=str, help="Description")
    parser.add_argument('--dataset', type=str, default='rocstories')
    parser.add_argument('--log_dir', type=str, default='log/')
    parser.add_argument('--save_dir', type=str, default='save/')
    parser.add_argument('--data_dir', type=str, default='data/')
    parser.add_argument('--submission_dir', type=str, default='submission/')
    parser.add_argument('--submit', action='store_true')
    parser.add_argument('--analysis', action='store_true')
    parser.add_argument('--fix_transformer', action='store_true')
    parser.add_argument('--seed', type=int, default=42)
    parser.add_argument('--n_iter', type=int, default=3)
    parser.add_argument('--n_batch', type=int, default=8)
    parser.add_argument('--max_grad_norm', type=int, default=1)
    parser.add_argument('--lr', type=float, default=6.25e-5)
    parser.add_argument('--lr_warmup', type=float, default=0.002)
    parser.add_argument('--n_ctx', type=int, default=512)
    parser.add_argument('--n_embd', type=int, default=768)
    parser.add_argument('--n_head', type=int, default=12)
    parser.add_argument('--n_layer', type=int, default=12)
    parser.add_argument('--embd_pdrop', type=float, default=0.1)
    parser.add_argument('--attn_pdrop', type=float, default=0.1)
    parser.add_argument('--resid_pdrop', type=float, default=0.1)
    parser.add_argument('--clf_pdrop', type=float, default=0.1)
    parser.add_argument('--l2', type=float, default=0.01)
    parser.add_argument('--vector_l2', action='store_true')
    parser.add_argument('--opt', type=str, default='adam')
    parser.add_argument('--afn', type=str, default='gelu')
    parser.add_argument('--lr_schedule', type=str, default='warmup_linear')
    parser.add_argument('--encoder_path', type=str, default='model/encoder_bpe_40000.json')
    parser.add_argument('--bpe_path', type=str, default='model/vocab_40000.bpe')
    parser.add_argument('--n_transfer', type=int, default=12)
    parser.add_argument('--lm_coef', type=float, default=0.5)
    parser.add_argument('--b1', type=float, default=0.9)
    parser.add_argument('--b2', type=float, default=0.999)
    parser.add_argument('--e', type=float, default=1e-8)
    parser.add_argument('--n_valid', type=int, default=374)
    parser.add_argument('--task_head', type=str, default='multiple_choice')
    parser.add_argument('--n_class', type=int, default=2)
    parser.add_argument('--debug', action='store_true')
    parser.add_argument('--gen_start_step', type=int, default=0)
    parser.add_argument('--checkpoint', type=int, default=10)

    args = parser.parse_args()
    print(args)

    random.seed(args.seed)
    np.random.seed(args.seed)
    torch.manual_seed(args.seed)
    torch.cuda.manual_seed_all(args.seed)

    # Constants
    submit = args.submit
    dataset = args.dataset
    n_ctx = args.n_ctx
    desc = args.desc

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    n_gpu = torch.cuda.device_count()
    print("device", device, "n_gpu", n_gpu)

    # logger = ResultLogger(path=os.path.join(log_dir, '{}.jsonl'.format(desc)), **args.__dict__)

    c_reader = CorpusReader(dataset, args.data_dir,
                            args.encoder_path,
                            args.bpe_path,
                            n_ctx,
                            debug=args.debug)
    log_dir = os.path.join(c_reader.corpus_dir, args.log_dir)
    submission_dir = os.path.join(c_reader.corpus_dir, args.submission_dir)
    save_dir = os.path.join(c_reader.corpus_dir, args.save_dir)
    os.makedirs(log_dir, exist_ok=True)
    os.makedirs(save_dir, exist_ok=True)
    os.makedirs(submission_dir, exist_ok=True)

    start_token = c_reader.corpus.start_token
    end_token = c_reader.corpus.end_token
    clf_token = c_reader.corpus.clf_token
    delimiter = c_reader.corpus.delimiter_token
    print("clf_token {}".format(clf_token))
    print("delimiter {}".format(delimiter))
    print("start_token {}".format(start_token))
    print("end_token {}".format(end_token))
    vocab = c_reader.corpus.n_vocab
    print("vocab {}".format(vocab))

    if args.task_head == 'generator':
        dh_model = TripleHeadModel(args, clf_token, args.task_head, vocab + n_ctx, n_ctx,
                                   delimiter=delimiter,
                                   end_token=end_token)
        generate = True
    else:
        if args.task_head == 'classification':
            task_head_type = ['classification', args.n_class]
        else:
            task_head_type = args.task_head
        dh_model = DoubleHeadModel(args, clf_token, task_head_type, vocab + n_ctx, n_ctx, delimiter=delimiter)
        generate = False

    if args.fix_transformer:
        for param in dh_model.transformer.parameters():
            param.requires_grad = False
    print('Parameter count: {}'.format(count_parameters(dh_model)))

    n_train = len(c_reader.corpus.data_encoded['train'])
    n_valid = len(c_reader.corpus.data_encoded['dev'])
    n_batch_train = args.n_batch * max(n_gpu, 1)
    n_updates_total = (n_train // n_batch_train) * args.n_iter
    writer = SummaryWriter(log_dir=log_dir)

    criterion = nn.CrossEntropyLoss(reduce=False)
    model_opt = OpenAIAdam(dh_model.parameters(),
                           lr=args.lr,
                           schedule=args.lr_schedule,
                           warmup=args.lr_warmup,
                           t_total=n_updates_total,
                           b1=args.b1,
                           b2=args.b2,
                           e=args.e,
                           l2=args.l2,
                           vector_l2=args.vector_l2,
                           max_grad_norm=args.max_grad_norm)

    if generate:
        compute_loss_fct = GenerationLossCompute(criterion,
                                                 criterion,
                                                 args.lm_coef,
                                                 model_opt,
                                                 generate_start_step=args.gen_start_step,
                                                 summary_writer=writer)
    else:
        compute_loss_fct = MultipleChoiceLossCompute(criterion,
                                                     criterion,
                                                     args.lm_coef,
                                                     model_opt,
                                                     summary_writer=writer)
    load_openai_pretrained_model(dh_model.transformer, n_ctx=n_ctx, n_special=len(c_reader.corpus.special))

    dh_model.to(device)
    dh_model = nn.DataParallel(dh_model)

    evaluator = Evaluator(c_reader, save_dir, writer, n_batch_train, generate=generate)

    n_updates = 0
    total_loss = 0
    save_interval = c_reader.corpus.data_len('train') // n_batch_train // args.checkpoint
    # save_interval = 10
    for i in range(args.n_iter):
        dh_model.train()
        for batch in tqdm(c_reader.corpus.batchify('train', batch_size=n_batch_train),
                          total=c_reader.corpus.data_len('train') // n_batch_train,
                          ncols=80,
                          desc="Epoch {}".format(i)):
            X = batch.documents.to(device)
            Y = batch.labels.to(device)
            MX = batch.masks.to(device)
            if generate:
                A = batch.answers.to(device)
                MA = batch.answers_masks.to(device)
                S = batch.spans.to(device)
                MS = batch.spans_masks.to(device)
                max_a_len = MS.long().sum(-1).max().item()
                d_logits = dh_model(X, S, max_a_len)
            else:
                d_logits = dh_model(X)

            logits = Logits(lm_logits=d_logits['lm_logits'],
                            clf_logits=d_logits['clf_logits'],
                            gen_logits=d_logits.get('gen_logits'))

            logits.clf_logits = logits.clf_logits.view((-1, logits.clf_logits.size(-1)))
            if generate:
                compute_loss_fct(X, Y.view(-1, ), MX, (A, MA),
                                 logits=logits, split='train', iteration=n_updates)
            else:
                compute_loss_fct(X, Y.view(-1, ), MX,
                                 logits=logits, split='train', iteration=n_updates)
            n_updates += 1
            if n_updates % save_interval == 0:
                evaluator.evaluate(dh_model, epoch=i, iteration=n_updates, split='dev')
                dh_model.train()
