import json
import math
import os
import sys
import time
from functools import partial

import numpy as np
import torch
import torch.nn as nn
from tqdm import tqdm


def encode_dataset(splits, encoder):
    encoded_splits = []
    for _, split in splits.items():
        fields = []
        for field in split:
            if isinstance(field[0], str):
                field = encoder.encode(field)
            elif isinstance(field[0], list):
                field = [encoder.encode(f, verbose=False) for f in field]
            fields.append(field)
        encoded_splits.append(fields)
    return encoded_splits


def encode_span_dataset(splits, encoder, span_idx=None, save_dir=None, max_len=None):
    if span_idx is None:
        span_idx = [0, -1]
    encoded_splits = []
    for name, split in splits.items():
        fields = []
        for idx, field in enumerate(split):
            if idx == span_idx[0]:
                if isinstance(field[0], str):
                    field, span = encoder.encode_span(field, split[2], split[span_idx[1]],
                                                      save_prefix=os.path.join(save_dir, name),
                                                      max_len=max_len)
            else:
                if isinstance(field[0], str):
                    field = encoder.encode(field)
                elif isinstance(field[0], list):
                    field = [encoder.encode(f, verbose=False) for f in field]
            fields.append(field)
        fields[-1] = span
        encoded_splits.append(fields)
    return encoded_splits


def stsb_label_encoding(labels, nclass=6):
    """
    Label encoding from Tree LSTM paper (Tai, Socher, Manning)
    """
    Y = np.zeros((len(labels), nclass)).astype(np.float32)
    for j, y in enumerate(labels):
        for i in range(nclass):
            if i == np.floor(y) + 1:
                Y[j, i] = y - np.floor(y)
            if i == np.floor(y):
                Y[j, i] = np.floor(y) - y + 1
    return Y


def np_softmax(x, t=1):
    x = x / t
    x = x - np.max(x, axis=-1, keepdims=True)
    ex = np.exp(x)
    return ex / np.sum(ex, axis=-1, keepdims=True)


def make_path(f):
    d = os.path.dirname(f)
    if d and not os.path.exists(d):
        os.makedirs(d)
    return f


def _identity_init(shape, dtype, partition_info, scale):
    n = shape[-1]
    w = np.eye(n) * scale
    if len([s for s in shape if s != 1]) == 2:
        w = w.reshape(shape)
    return w.astype(np.float32)


def identity_init(scale=1.0):
    return partial(_identity_init, scale=scale)


def _np_init(shape, dtype, partition_info, w):
    return w


def np_init(w):
    return partial(_np_init, w=w)


class ResultLogger(object):
    def __init__(self, path, *args, **kwargs):
        if 'time' not in kwargs:
            kwargs['time'] = time.time()
        self.f_log = open(make_path(path), 'w')
        self.f_log.write(json.dumps(kwargs) + '\n')

    def log(self, **kwargs):
        if 'time' not in kwargs:
            kwargs['time'] = time.time()
        self.f_log.write(json.dumps(kwargs) + '\n')
        self.f_log.flush()

    def close(self):
        self.f_log.close()


def flatten(outer):
    return [el for inner in outer for el in inner]


def remove_none(l):
    return [e for e in l if e is not None]


def iter_data(*datas, n_batch=128, truncate=False, verbose=False, max_batches=float("inf"), desc=''):
    n = len(datas[0])
    if truncate:
        n = (n // n_batch) * n_batch
    n = min(n, max_batches * n_batch)
    n_batches = 0
    if verbose:
        f = sys.stderr
    else:
        f = open(os.devnull, 'w')
    for i in tqdm(range(0, n, n_batch), total=n // n_batch, file=f, ncols=80, leave=False, desc=desc):
        if n_batches >= max_batches: raise StopIteration
        if len(datas) == 1:
            yield datas[0][i:i + n_batch]
        else:
            yield (d[i:i + n_batch] for d in datas)
        n_batches += 1


def convert_to_one_hot(indices, num_classes):
    """
    Args:
        indices (tensor): A vector containing indices,
            whose size is (batch_size,).
        num_classes (tensor): The number of classes, which would be
            the second dimension of the resulting one-hot matrix.

    Returns:
        result: The one-hot matrix of size (batch_size, num_classes).
    """

    batch_size = indices.size(0)
    indices = indices.unsqueeze(1)
    one_hot = indices.new_zeros(batch_size, num_classes).scatter_(1, indices, 1)
    return one_hot


def st_gumbel_softmax(masked_logits, temperature=1.0):
    """
    Return the result of Straight-Through Gumbel-Softmax Estimation.
    It approximates the discrete sampling via Gumbel-Softmax trick
    and applies the biased ST estimator.
    In the forward propagation, it emits the discrete one-hot result,
    and in the backward propagation it approximates the categorical
    distribution via smooth Gumbel-Softmax distribution.

    Args:
        masked_logits (tensor): A un-normalized probability values,
            which has the size (batch_size, num_classes)
        temperature (float): A temperature parameter. The higher
            the value is, the smoother the distribution is.

    Returns:
        y: The sampled output, which has the property explained above.
    """

    eps = 1e-20
    u = masked_logits.data.new(*masked_logits.size()).uniform_()
    gumbel_noise = -torch.log(-torch.log(u + eps) + eps)
    y = masked_logits + gumbel_noise
    probs = torch.softmax(y / temperature, dim=1)
    y_argmax = probs.max(1)[1]
    y_hard = convert_to_one_hot(indices=y_argmax, num_classes=y.size(1)).float()
    y = (y_hard - y).detach() + y
    return y


def greedy_select(masked_logits):
    probs = torch.softmax(masked_logits, dim=1)
    one_hot = convert_to_one_hot(indices=probs.max(1)[1],
                                 num_classes=masked_logits.size(1))
    return one_hot


INF = 1e30


def gelu(x):
    return 0.5 * x * (1 + torch.tanh(math.sqrt(2 / math.pi) * (x + 0.044715 * torch.pow(x, 3))))


def swish(x):
    return x * torch.sigmoid(x)


def softmax_mask(val, mask):
    return -INF * (1 - mask.float()) + val


ACT_FNS = {
    'relu': nn.ReLU,
    'swish': swish,
    'gelu': gelu
}
