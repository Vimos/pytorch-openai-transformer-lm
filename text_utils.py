import json
import re

import ftfy
import spacy
import numpy as np
from tqdm import tqdm


def get_pairs(word):
    """
    Return set of symbol pairs in a word.
    word is represented as tuple of symbols (symbols being variable-length strings)
    """
    pairs = set()
    prev_char = word[0]
    for char in word[1:]:
        pairs.add((prev_char, char))
        prev_char = char
    return pairs


def text_standardize(text):
    """
    fixes some issues the spacy tokenizer had on books corpus
    also does some whitespace standardization
    """
    text = text.replace('—', '-')
    text = text.replace('–', '-')
    text = text.replace('―', '-')
    text = text.replace('…', '...')
    text = text.replace('´', "'")
    text = re.sub(r'''(-+|~+|!+|"+|;+|\?+|\++|,+|\)+|\(+|\\+|\/+|\*+|\[+|\]+|}+|{+|\|+|_+)''', r' \1 ', text)
    text = re.sub(r'\s*\n\s*', ' \n ', text)
    text = re.sub(r'[^\S\n]+', ' ', text)
    return text.strip()


class TextEncoder(object):
    """
    mostly a wrapper for a public python bpe tokenizer
    """

    def __init__(self, encoder_path, bpe_path):
        self.nlp = spacy.load('en', disable=['parser', 'tagger', 'ner', 'textcat'])
        self.encoder = json.load(open(encoder_path))
        self.decoder = {v: k for k, v in self.encoder.items()}
        merges = open(bpe_path, encoding='utf-8').read().split('\n')[1:-1]
        merges = [tuple(merge.split()) for merge in merges]
        self.bpe_ranks = dict(zip(merges, range(len(merges))))
        self.cache = {}

    def bpe(self, token):
        word = tuple(token[:-1]) + (token[-1] + '</w>',)
        if token in self.cache:
            return self.cache[token]
        pairs = get_pairs(word)

        if not pairs:
            return token + '</w>'

        while True:
            bigram = min(pairs, key=lambda pair: self.bpe_ranks.get(pair, float('inf')))
            if bigram not in self.bpe_ranks:
                break
            first, second = bigram
            new_word = []
            i = 0
            while i < len(word):
                try:
                    j = word.index(first, i)
                    new_word.extend(word[i:j])
                    i = j
                except:
                    new_word.extend(word[i:])
                    break

                if word[i] == first and i < len(word) - 1 and word[i + 1] == second:
                    new_word.append(first + second)
                    i += 2
                else:
                    new_word.append(word[i])
                    i += 1
            new_word = tuple(new_word)
            word = new_word
            if len(word) == 1:
                break
            else:
                pairs = get_pairs(word)
        word = ' '.join(word)
        if word == '\n  </w>':
            word = '\n</w>'
        self.cache[token] = word
        return word

    def encode(self, texts, verbose=True):
        texts_tokens = []
        if verbose:
            for text in tqdm(texts, ncols=80, desc='encode'):
                text = self.nlp(text_standardize(ftfy.fix_text(text)))
                text_tokens = []
                for token in text:
                    text_tokens.extend([self.encoder.get(t, 0) for t in self.bpe(token.text.lower()).split(' ')])
                texts_tokens.append(text_tokens)
        else:
            for text in texts:
                text = self.nlp(text_standardize(ftfy.fix_text(text)))
                text_tokens = []
                for token in text:
                    text_tokens.extend([self.encoder.get(t, 0) for t in self.bpe(token.text.lower()).split(' ')])
                texts_tokens.append(text_tokens)
        return texts_tokens

    def encode_span(self, texts, answers, spans, save_prefix=None, max_len=None):
        texts_tokens = []
        tokens_list = []
        word_span = []
        for text, answer, span in tqdm(zip(texts, answers, spans), total=len(texts), ncols=80, desc='encode'):
            text = self.nlp(text_standardize(ftfy.fix_text(text)))
            answer_list = [t.text for t in self.nlp(text_standardize(ftfy.fix_text(answer)))]
            text_list = [t.text for t in text]

            left, right = None, None
            try:
                while answer_list[-1] == '.':
                    answer_list.pop()
                for i, w in enumerate(text_list):
                    if i + len(answer_list) <= len(text) and answer_list[0] in w and answer_list[-1] in \
                            text_list[i + len(answer_list) - 1]:
                        left = i
                        right = i + len(answer_list)
                if left is None or right is None:
                    print(answer_list, text_list, left, right)
            except Exception as e:
                print(answer_list, text_list, e)

            text_tokens = []
            token_list = []
            new_left, new_right = 0, 0
            for i, token in enumerate(text):
                bpe_list = self.bpe(token.text.lower()).split(' ')
                if i == left:
                    new_left = len(token_list)
                if i == right:
                    new_right = len(token_list) + len(bpe_list) - 1
                if new_right > max_len:
                    shift_len = new_right + 25 - max_len
                    bpe_list = bpe_list[shift_len:max_len + shift_len]
                    new_right = new_right - shift_len
                    new_left = new_left - shift_len

                text_tokens.extend([self.encoder.get(t, 0) for t in bpe_list])
                token_list.extend(bpe_list)
            texts_tokens.append(text_tokens)
            tokens_list.append(token_list)
            # print(token_list[new_left:new_right], answer_list)

            word_span.append((new_left + 1, new_right + 1))

        with open("{}_tokes.json".format(save_prefix), 'w') as f:
            json.dump(tokens_list, f)
        return texts_tokens, word_span
