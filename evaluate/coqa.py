import json
import os

import numpy as np
import re
from collections import Counter
import string

import torch

file_path = os.path.dirname(__file__)
coqa_dir = os.path.join(file_path, '..', 'data', 'coqa')


def convert_tokens(eval_file, qa_id, pp1, pp2):
    answer_dict = {}
    remapped_dict = {}
    for qid, p1, p2 in zip(qa_id, pp1, pp2):
        context = eval_file[str(qid)]["context"]
        spans = eval_file[str(qid)]["spans"]
        uuid = eval_file[str(qid)]["uuid"]
        start_idx = spans[p1][0]
        end_idx = spans[p2][1]
        answer_dict[str(qid)] = context[start_idx: end_idx]
        remapped_dict[uuid] = context[start_idx: end_idx]
    return answer_dict, remapped_dict


def evaluate(eval_file, answer_dict):
    f1 = exact_match = total = 0
    for key, value in answer_dict.items():
        total += 1
        ground_truths = eval_file[key]["answers"]
        prediction = value
        exact_match += metric_max_over_ground_truths(
            exact_match_score, prediction, ground_truths)
        f1 += metric_max_over_ground_truths(f1_score,
                                            prediction, ground_truths)
    exact_match = 100.0 * exact_match / total
    f1 = 100.0 * f1 / total
    return {'exact_match': exact_match, 'f1': f1}


def normalize_answer(s):
    def remove_articles(text):
        return re.sub(r'\b(a|an|the)\b', ' ', text)

    def white_space_fix(text):
        return ' '.join(text.split())

    def remove_punc(text):
        exclude = set(string.punctuation)
        return ''.join(ch for ch in text if ch not in exclude)

    def lower(text):
        return text.lower()

    return white_space_fix(remove_articles(remove_punc(lower(s))))


def f1_score(prediction, ground_truth):
    prediction_tokens = normalize_answer(prediction).split()
    ground_truth_tokens = normalize_answer(ground_truth).split()
    common = Counter(prediction_tokens) & Counter(ground_truth_tokens)
    num_same = sum(common.values())
    if num_same == 0:
        return 0
    precision = 1.0 * num_same / len(prediction_tokens)
    recall = 1.0 * num_same / len(ground_truth_tokens)
    f1 = (2 * precision * recall) / (precision + recall)
    return f1


def exact_match_score(prediction, ground_truth):
    return (normalize_answer(prediction) == normalize_answer(ground_truth))


def metric_max_over_ground_truths(metric_fn, prediction, ground_truths):
    scores_for_ground_truths = []
    for ground_truth in ground_truths:
        score = metric_fn(prediction, ground_truth)
        scores_for_ground_truths.append(score)
    return max(scores_for_ground_truths)


if __name__ == '__main__':
    source_path = os.path.join(coqa_dir, "CoQA", "coqa-dev-v1.0.json")
    source_data = json.load(open(source_path, 'r'))

    with open(os.path.join(coqa_dir, 'save', 'dev_tokes.json'),
              'r', encoding='utf-8') as fpw:
        tokens_list = json.load(fpw)

    count = 0

    answers = []
    for i, d in enumerate(source_data['data']):
        for question, answer in zip(d['questions'], d['answers']):
            answers.append([answer['span_text']])

    save_dir = os.path.join(coqa_dir, 'save')
    for pred in os.scandir(save_dir):
        if pred.name.endswith('.json'):
            continue
        print("=================={}=================".format(pred.name))
        f1 = exact_match = total = 0
        d = torch.load(pred.path)

        for tokens, answer, span in zip(tokens_list, answers, d):
            start = np.argmax(span[0]) - 1
            end = np.argmax(span[1]) - 1
            total += 1
            prediction = ' '.join(''.join(tokens[start:end]).split('</w>'))
            # if prediction:
            #     print(answer, prediction)
            exact_match += metric_max_over_ground_truths(
                exact_match_score, prediction, answer)
            f1 += metric_max_over_ground_truths(f1_score,
                                                prediction, answer)
        exact_match = 100.0 * exact_match / total
        f1 = 100.0 * f1 / total
        print({'exact_match': exact_match, 'f1': f1})
