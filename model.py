import collections
import copy
import json
import math
import re

import numpy as np
import torch
import torch.nn as nn
from torch.autograd import Variable
from torch.nn.parameter import Parameter

from utils import st_gumbel_softmax, greedy_select, ACT_FNS, softmax_mask, INF


class LayerNorm(nn.Module):
    "Construct a layernorm module in the OpenAI style (epsilon inside the square root)."

    def __init__(self, n_state, e=1e-5):
        super(LayerNorm, self).__init__()
        self.g = nn.Parameter(torch.ones(n_state))
        self.b = nn.Parameter(torch.zeros(n_state))
        self.e = e

    def forward(self, x):
        u = x.mean(-1, keepdim=True)
        s = (x - u).pow(2).mean(-1, keepdim=True)
        x = (x - u) / torch.sqrt(s + self.e)
        return self.g * x + self.b


class Conv1D(nn.Module):
    def __init__(self, nf, rf, nx):
        super(Conv1D, self).__init__()
        self.rf = rf
        self.nf = nf
        if rf == 1:  # faster 1x1 conv
            w = torch.empty(nx, nf)
            nn.init.normal_(w, std=0.02)
            self.w = Parameter(w)
            self.b = Parameter(torch.zeros(nf))
        else:  # was used to train LM
            raise NotImplementedError

    def forward(self, x):
        if self.rf == 1:
            size_out = x.size()[:-1] + (self.nf,)
            x = torch.addmm(self.b, x.view(-1, x.size(-1)), self.w)
            x = x.view(*size_out)
        else:
            raise NotImplementedError
        return x


class Attention(nn.Module):
    def __init__(self, nx, n_ctx, cfg, scale=False):
        super(Attention, self).__init__()
        n_state = nx  # in Attention: n_state=768 (nx=n_embd)
        # [switch nx => n_state from Block to Attention to keep identical to TF implem]
        assert n_state % cfg.n_head == 0
        self.register_buffer('b', torch.tril(torch.ones(n_ctx, n_ctx)).view(1, 1, n_ctx, n_ctx))
        self.n_head = cfg.n_head
        self.split_size = n_state
        self.scale = scale
        self.c_attn = Conv1D(n_state * 3, 1, nx)
        self.c_proj = Conv1D(n_state, 1, nx)
        self.attn_dropout = nn.Dropout(cfg.attn_pdrop)
        self.resid_dropout = nn.Dropout(cfg.resid_pdrop)

    def _attn(self, q, k, v):
        w = torch.matmul(q, k)
        if self.scale:
            w = w / math.sqrt(v.size(-1))
        w = w * self.b + -1e9 * (1 - self.b)  # TF implem method: mask_attn_weights
        w = nn.Softmax(dim=-1)(w)
        w = self.attn_dropout(w)
        return torch.matmul(w, v)

    def merge_heads(self, x):
        x = x.permute(0, 2, 1, 3).contiguous()
        new_x_shape = x.size()[:-2] + (x.size(-2) * x.size(-1),)
        return x.view(*new_x_shape)  # in Tensorflow implem: fct merge_states

    def split_heads(self, x, k=False):
        new_x_shape = x.size()[:-1] + (self.n_head, x.size(-1) // self.n_head)
        x = x.view(*new_x_shape)  # in Tensorflow implem: fct split_states
        if k:
            return x.permute(0, 2, 3, 1)
        else:
            return x.permute(0, 2, 1, 3)

    def forward(self, x):
        x = self.c_attn(x)
        query, key, value = x.split(self.split_size, dim=2)
        query = self.split_heads(query)
        key = self.split_heads(key, k=True)
        value = self.split_heads(value)
        a = self._attn(query, key, value)
        a = self.merge_heads(a)
        a = self.c_proj(a)
        a = self.resid_dropout(a)
        return a


class MLP(nn.Module):
    def __init__(self, n_state, cfg):  # in MLP: n_state=3072 (4 * n_embd)
        super(MLP, self).__init__()
        nx = cfg.n_embd
        self.c_fc = Conv1D(n_state, 1, nx)
        self.c_proj = Conv1D(nx, 1, n_state)
        self.act = ACT_FNS[cfg.afn]
        self.dropout = nn.Dropout(cfg.resid_pdrop)

    def forward(self, x):
        h = self.act(self.c_fc(x))
        h2 = self.c_proj(h)
        return self.dropout(h2)


class Block(nn.Module):
    def __init__(self, n_ctx, cfg, scale=False):
        super(Block, self).__init__()
        nx = cfg.n_embd
        self.attn = Attention(nx, n_ctx, cfg, scale)
        self.ln_1 = LayerNorm(nx)
        self.mlp = MLP(4 * nx, cfg)
        self.ln_2 = LayerNorm(nx)

    def forward(self, x):
        a = self.attn(x)
        n = self.ln_1(x + a)
        m = self.mlp(n)
        h = self.ln_2(n + m)
        return h


class TransformerModel(nn.Module):
    """ Transformer model """

    def __init__(self, cfg, vocab=40990, n_ctx=512):
        super(TransformerModel, self).__init__()
        self.vocab = vocab
        self.embed = nn.Embedding(vocab, cfg.n_embd)
        self.drop = nn.Dropout(cfg.embd_pdrop)
        block = Block(n_ctx, cfg, scale=True)
        self.h = nn.ModuleList([copy.deepcopy(block) for _ in range(cfg.n_layer)])
        self.decoder = nn.Linear(cfg.n_embd, vocab, bias=False)
        self.decoder.weight = self.embed.weight  # Tied weights

        nn.init.normal_(self.embed.weight, std=0.02)

    def forward(self, x):
        x = x.view(-1, x.size(-2), x.size(-1))
        e = self.embed(x)
        # Add the position information to the input embeddings
        h = e.sum(dim=2)
        for block in self.h:
            h = block(h)
        return h


class LMHead(nn.Module):
    """ Language Model Head for the transformer """

    def __init__(self, model, cfg):
        super(LMHead, self).__init__()
        self.n_embd = cfg.n_embd
        embed_shape = model.embed.weight.shape
        self.decoder = nn.Linear(embed_shape[1], embed_shape[0], bias=False)
        self.decoder.weight = model.embed.weight  # Tied weights

    def forward(self, h):
        # Truncated Language modeling logits (we remove the last token)
        h_trunc = h[:, :-1].contiguous().view(-1, self.n_embd)
        lm_logits = self.decoder(h_trunc)
        return lm_logits


class MultipleChoiceHead(nn.Module):
    """ Classifier Head for the transformer """

    def __init__(self, clf_token, cfg):
        super(MultipleChoiceHead, self).__init__()
        self.n_embd = cfg.n_embd
        self.clf_token = clf_token
        self.dropout = nn.Dropout2d(cfg.clf_pdrop)  # To reproduce the noise_shape parameter of TF implementation
        self.linear = nn.Linear(cfg.n_embd, 1)

        nn.init.normal_(self.linear.weight, std=0.02)
        nn.init.normal_(self.linear.bias, 0)

    def forward(self, h, x):
        # Classification logits
        clf_h = h.view(-1, self.n_embd)
        flat = x[..., 0].contiguous().view(-1)
        clf_h = clf_h[flat == self.clf_token, :]
        clf_h = clf_h.view(-1, x.size(1), self.n_embd, 1)
        # This double transposition is there to replicate the behavior
        # of the noise_shape argument in the tensorflow
        # implementation.  For more details, see
        # https://github.com/huggingface/pytorch-openai-transformer-lm/issues/11
        clf_h = self.dropout(clf_h.transpose(1, 2)).transpose(1, 2)
        clf_h = clf_h.contiguous().view(-1, self.n_embd)
        clf_logits = self.linear(clf_h)

        return clf_logits.view(-1, x.size(1))


class ClfHead(nn.Module):
    """Classification Head for the transformer

    TODO: test this class."""

    def __init__(self, clf_token, cfg, n_class):
        super(ClfHead, self).__init__()
        self.n_embd = cfg.n_embd
        self.clf_token = clf_token
        self.dropout = nn.Dropout(cfg.clf_pdrop)
        self.linear = nn.Linear(cfg.n_embd, n_class)

        nn.init.normal_(self.linear.weight, std=0.02)
        nn.init.normal_(self.linear.bias, 0)

    def forward(self, h, x):
        clf_h = h.view(-1, self.n_embd)
        flat = x[..., 0].contiguous().view(-1)
        clf_h = clf_h[flat == self.clf_token, :]
        clf_h = self.dropout(clf_h)
        clf_logits = self.linear(clf_h)

        return clf_logits


class SimilarityHead(nn.Module):
    """ Similarity Head for the transformer

        TODO: test this class."""

    def __init__(self, clf_token, cfg):
        super(SimilarityHead, self).__init__()
        self.n_embd = cfg.n_embd
        self.clf_token = clf_token
        self.dropout = nn.Dropout(cfg.clf_pdrop)
        self.linear = nn.Linear(cfg.n_embd, 1)

        nn.init.normal_(self.linear.weight, std=0.02)
        nn.init.normal_(self.linear.bias, 0)

    def forward(self, h, x):
        sim_h = h.view(-1, self.n_embd)
        flat = x[..., 0].contiguous().view(-1)
        sim_h = sim_h[flat == self.clf_token, :]
        sim_h = self.dropout(sim_h)
        sim_h = sim_h.sum(dim=1)
        sim_logits = self.linear(sim_h)

        return sim_logits


class DotAttention(nn.Module):
    def __init__(self, mem_dim, dropoutP):
        super(DotAttention, self).__init__()
        self.trans_linear = nn.Linear(mem_dim, mem_dim)
        self.drop_module = nn.Dropout(dropoutP)

    def forward(self, inputs):
        proj_p, proj_q, mask = inputs
        proj_q = proj_q.unsqueeze(1)
        trans_q = self.trans_linear(proj_q)
        att_weights = proj_p.bmm(torch.transpose(trans_q, 1, 2)).squeeze()
        att_norm = softmax_mask(att_weights, mask)

        att_vec = torch.softmax(att_norm, dim=1).unsqueeze(1).bmm(proj_p).squeeze(1)
        # elem_min = att_vec - proj_q
        # elem_mul = att_vec * proj_q
        # all_con = torch.cat([elem_min, elem_mul], 2)
        # output = nn.ReLU()(self.ln_1(self.map_linear(all_con)))
        return att_vec, att_norm


def masked_softmax(vector, seq_lens):
    mask = vector.new(vector.size()).zero_()
    for i in range(seq_lens.size(0)):
        mask[i, :, :seq_lens[i]] = 1
    mask = Variable(mask, requires_grad=False)

    if mask is None:
        result = torch.nn.functional.softmax(vector, dim=-1)
    else:
        result = torch.nn.functional.softmax(vector * mask, dim=-1)
        result = result * mask
        result = result / (result.sum(dim=-1, keepdim=True) + 1e-13)
    return result


class MatchNet(nn.Module):
    def __init__(self, mem_dim, dropoutP):
        super(MatchNet, self).__init__()
        self.map_linear = nn.Linear(2 * mem_dim, mem_dim)
        self.trans_linear = nn.Linear(mem_dim, mem_dim)
        self.drop_module = nn.Dropout(dropoutP)

    def forward(self, inputs):
        proj_p, proj_q, seq_len = inputs
        trans_q = self.trans_linear(proj_q)
        att_weights = proj_p.bmm(torch.transpose(proj_q, 1, 2))
        att_norm = masked_softmax(att_weights, seq_len)

        att_vec = att_norm.bmm(proj_q)
        elem_min = att_vec - proj_p
        elem_mul = att_vec * proj_p
        all_con = torch.cat([elem_min, elem_mul], 2)
        output = nn.ReLU()(self.map_linear(all_con))
        return output


def shift_span(h, span_mask):
    # print('-----------------------------------------')
    # print('h = ', h)
    # print(span_mask)
    b = span_mask.cumsum(1).long()
    # print('b = ', b)

    c = (1 - (b > 0)).long()  # compute left unmask
    # print('c = ', c)

    d = c.cumsum(1)
    # print('d = ', d)

    e = (d + c.sum(1).unsqueeze(1) - 1) + b  # left unmask len to be gathered
    e[e >= h.size(1)] = 0
    # print('e = ', e)

    f = e.unsqueeze(2).expand_as(h)  # expand to same size as h
    # print('f = ', f)

    g = span_mask.gather(1, e)
    # print('g = ', g)
    return h.gather(1, f) * g.unsqueeze(2).expand_as(h).float(), g


class PointerHead(nn.Module):
    def __init__(self, clf_token, cfg, delimiter=None, self_matching=False):
        super().__init__()
        self.n_embd = cfg.n_embd
        self.clf_token = clf_token
        self.delimiter = delimiter
        self.self_matching = self_matching
        self.dropout = nn.Dropout(cfg.clf_pdrop)
        self.match = MatchNet(self.n_embd, cfg.clf_pdrop)
        self.self_match = MatchNet(self.n_embd, cfg.clf_pdrop)
        self.trans_linear = nn.Linear(self.n_embd * 2, self.n_embd * 2)
        self.rank_module = nn.Linear(self.n_embd * 2, 1)
        self.gru = torch.nn.GRUCell(input_size=self.n_embd, hidden_size=self.n_embd)

    def pointer(self, inputs, state, mask):
        u = torch.cat([state.unsqueeze(1).repeat(1, inputs.size(1), 1), inputs], 2)
        s0 = torch.tanh(self.trans_linear(u))
        s = self.rank_module(s0)
        s1 = softmax_mask(s.squeeze(2), mask)
        a = torch.softmax(s1.unsqueeze(1), dim=2)
        res = a.bmm(inputs).squeeze(1)
        return res, s1

    def forward(self, h, x):
        sim_h = h.view(-1, self.n_embd)
        flat = x[..., 0].contiguous().view(-1)
        sim_h = sim_h[flat == self.clf_token, :]

        m_d = (flat == self.delimiter).view(x.size(0), -1).long()
        m_c = (flat == self.clf_token).view(x.size(0), -1).long()

        d_right_mask = m_d.cumsum(1)
        p_span_mask = 1 - d_right_mask + m_d

        c_left_mask = 1 - m_c.cumsum(1)
        q_span_mask = c_left_mask - p_span_mask

        max_p_len = p_span_mask.sum(1).max().item()
        max_q_len = q_span_mask.sum(1).max().item()

        q_shift, q_shift_mask = shift_span(h, q_span_mask)

        p_slice = h.narrow(1, 0, max_p_len)
        q_slice = q_shift.narrow(1, 0, max_q_len)

        p = self.dropout(p_slice)
        q = self.dropout(q_slice)
        p_mask = p_span_mask.narrow(1, 0, max_p_len)

        if self.self_matching:
            pq_matching = self.match([p, q, q_span_mask.sum(1)])
            matching = self.self_match([pq_matching, pq_matching, p_span_mask.sum(1)])
        else:
            matching = self.match([p, q, q_span_mask.sum(1)])

        att_vec, left_logits = self.pointer(matching, sim_h, p_mask)
        state = self.gru(att_vec, sim_h)
        _, right_logits = self.pointer(matching, state, p_mask)

        logits = torch.stack([left_logits, right_logits], 1)
        p2d = (0, h.size(1) - max_p_len, 0, 0)
        return (p, sim_h, logits), torch.nn.functional.pad(logits, p2d, 'constant', -INF)


class GeneratorHead(nn.Module):
    def __init__(self, model, cfg, clf_token, end_token):
        super(GeneratorHead, self).__init__()
        self.n_embd = cfg.n_embd
        self.clf_token = clf_token
        self.end_token = end_token
        embed_shape = model.embed.weight.shape
        self.decoder = nn.Linear(embed_shape[1], embed_shape[0], bias=False)
        self.decoder.weight = model.embed.weight  # Tied weights
        self.linear_out = nn.Linear(self.n_embd * 2, self.n_embd)

    def get_span_mask(self, left_logits, right_logits):
        if self.training:
            left_span = st_gumbel_softmax(masked_logits=left_logits, temperature=1)
        else:
            left_span = greedy_select(masked_logits=left_logits)

        left_span_mask = (1 - left_span.cumsum(1))
        new_right_logits = (left_span + left_span_mask).float() * (-INF) + right_logits * (
                left_span.cumsum(1) > 0).float()

        if self.training:
            right_span = st_gumbel_softmax(masked_logits=new_right_logits, temperature=1)
        else:
            right_span = greedy_select(masked_logits=new_right_logits)
        right_span_mask = (1 - right_span.cumsum(1))
        span_mask = right_span_mask - left_span_mask
        if (span_mask < 0).sum() > 0:
            return left_span_mask
        else:
            return span_mask

    def forward(self, p, logits, sim_h, h_s, s, max_a_len):
        # span_mask = self.get_span_mask(logits[:, 0, :], logits[:, 1, :])
        # max_a_len = span_mask.long().sum(1).max().item()

        # gen, gen_mask = shift_span(p, span_mask)

        # gen_narrow = gen.narrow(1, 0, max_a_len)
        # gen_mask_narrow = gen_mask.narrow(1, 0, max_a_len)

        sim_h = h_s.view(-1, self.n_embd)
        flat = s[..., 0].contiguous().view(-1)
        sim_h = sim_h[flat == self.end_token, :]

        # m_s = (flat == self.end_token).view(s.size(0), -1).long()
        # d_right_mask = m_s.cumsum(1)
        # span_mask = 1 - d_right_mask + m_s
        # max_a_len = span_mask.long().sum(1).max().item()
        p_slice = h_s.narrow(1, 0, max_a_len)
        sim_h_repeat = sim_h.unsqueeze(1).repeat(1, max_a_len, 1)

        # concat -> (batch, out_len, 2*dim)
        combined = torch.cat([p_slice, sim_h_repeat], 2)
        output = torch.relu(self.linear_out(combined.view(-1, 2 * self.n_embd))).view(s.size(0), -1, self.n_embd)

        h_trunc = output.contiguous().view(-1, self.n_embd)
        lm_logits = self.decoder(h_trunc)
        return lm_logits


class DoubleHeadModel(nn.Module):
    """ Transformer with language model and task specific heads """

    def __init__(self, cfg, clf_token, task_head_type, vocab=40990, n_ctx=512, delimiter=None):
        super(DoubleHeadModel, self).__init__()
        self.transformer = TransformerModel(cfg, vocab=vocab, n_ctx=n_ctx)
        self.lm_head = LMHead(self.transformer, cfg)
        if isinstance(task_head_type, str):
            if task_head_type == 'multiple_choice':
                self.task_head = MultipleChoiceHead(clf_token, cfg)
            elif task_head_type == 'similarity':
                self.task_head = SimilarityHead(clf_token, cfg)
            elif task_head_type == 'inference':
                # the three classes correspond to entailment, contradiction and neutral.
                self.task_head = ClfHead(clf_token, cfg, 2)
            elif task_head_type == 'pointer':
                self.task_head = PointerHead(clf_token, cfg, delimiter=delimiter)
            else:
                raise ValueError("task_head_type is expected to be 'multiple_choice' "
                                 "'similarity', 'inference' or ('classification', n_class) "
                                 f"got {task_head_type}.")
        elif isinstance(task_head_type, collections.abc.Sequence) and len(task_head_type) == 2 and \
                task_head_type[0] == 'classification':
            n_class = task_head_type[1]
            self.task_head = ClfHead(clf_token, cfg, n_class)
        else:
            raise ValueError("task_head_type is expected to be 'multiple_choice' "
                             "'similarity', 'inference' or ('classification', n_class) "
                             f"got {task_head_type}.")

    def forward(self, x):
        h = self.transformer(x)
        lm_logits = self.lm_head(h)
        _, clf_logits = self.task_head(h, x)

        return {"lm_logits": lm_logits, "clf_logits": clf_logits}


class TripleHeadModel(nn.Module):
    """ Transformer with language model and task specific heads """

    def __init__(self, cfg, clf_token, task_head_type, vocab=40990, n_ctx=512, delimiter=None, end_token=None):
        super(TripleHeadModel, self).__init__()
        self.transformer = TransformerModel(cfg, vocab=vocab, n_ctx=n_ctx)
        self.lm_head = LMHead(self.transformer, cfg)
        self.task_head = PointerHead(clf_token, cfg, delimiter=delimiter)
        self.gen_head = GeneratorHead(self.transformer, cfg, clf_token, end_token=end_token)

    def forward(self, x, s, max_a_len):
        h = self.transformer(x)
        h_s = self.transformer(s)
        lm_logits = self.lm_head(h)
        (p, sim_h, clf_logits), clf_logits_padded = self.task_head(h, x)
        gen_logits = self.gen_head(p, clf_logits, sim_h, h_s, s, max_a_len)

        return {"lm_logits": lm_logits, "clf_logits": clf_logits_padded, "gen_logits": gen_logits}


def load_openai_pretrained_model(model, n_ctx=-1, n_special=-1, n_transfer=12, n_embd=768, path='./model/',
                                 path_names='./'):
    # Load weights from TF model
    print("Loading weights...")
    names = json.load(open(path_names + 'parameters_names.json'))
    shapes = json.load(open(path + 'params_shapes.json'))
    offsets = np.cumsum([np.prod(shape) for shape in shapes])
    init_params = [np.load(path + 'params_{}.npy'.format(n)) for n in range(10)]
    init_params = np.split(np.concatenate(init_params, 0), offsets)[:-1]
    init_params = [param.reshape(shape) for param, shape in zip(init_params, shapes)]
    if n_ctx > 0:
        init_params[0] = init_params[0][:n_ctx]
    if n_special > 0:
        init_params[0] = np.concatenate(
            [init_params[1],
             (np.random.randn(n_special, n_embd) * 0.02).astype(np.float32),
             init_params[0]
             ], 0)
    else:
        init_params[0] = np.concatenate(
            [init_params[1],
             init_params[0]
             ], 0)
    del init_params[1]
    if n_transfer == -1:
        n_transfer = 0
    else:
        n_transfer = 1 + n_transfer * 12
    init_params = [arr.squeeze() for arr in init_params]

    try:
        assert model.embed.weight.shape == init_params[0].shape
    except AssertionError as e:
        e.args += (model.embed.weight.shape, init_params[0].shape)
        raise

    model.embed.weight.data = torch.from_numpy(init_params[0])

    for name, ip in zip(names[1:n_transfer], init_params[1:n_transfer]):
        name = name[6:]  # skip "model/"
        assert name[-2:] == ":0"
        name = name[:-2]
        name = name.split('/')
        pointer = model
        for m_name in name:
            if re.fullmatch(r'[A-Za-z]+\d+', m_name):
                l = re.split(r'(\d+)', m_name)
            else:
                l = [m_name]
            pointer = getattr(pointer, l[0])
            if len(l) >= 2:
                num = int(l[1])
                pointer = pointer[num]
        try:
            assert pointer.shape == ip.shape
        except AssertionError as e:
            e.args += (pointer.shape, ip.shape)
            raise
        pointer.data = torch.from_numpy(ip)


class dotdict(dict):
    """dot.notation access to dictionary attributes"""
    __getattr__ = dict.get
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__


DEFAULT_CONFIG = dotdict({
    'n_embd': 768,
    'n_head': 12,
    'n_layer': 12,
    'embd_pdrop': 0.1,
    'attn_pdrop': 0.1,
    'resid_pdrop': 0.1,
    'afn': 'gelu',
    'clf_pdrop': 0.1})
