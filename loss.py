import torch


class MultipleChoiceLossCompute:
    "A Loss compute and train function for multiple choice tasks."

    def __init__(self, lm_criterion, clf_criterion, lm_coef, opt=None, summary_writer=None):
        self.summary_writer = summary_writer
        self.lm_criterion = lm_criterion
        self.clf_criterion = clf_criterion
        self.lm_coef = lm_coef
        self.opt = opt

    def __call__(self, X, Y, M, logits=None, only_return_losses=False, iteration=0, split=None):
        # Language modeling loss
        if logits.lm_logits is not None:
            x_shifted = X[:, :, 1:, 0].contiguous().view(-1)  # Shape: 252
            M = M.view(-1, M.size(2))
            lm_losses = self.lm_criterion(logits.lm_logits, x_shifted)
            lm_losses = lm_losses.view(X.size(0) * X.size(1), X.size(2) - 1)
            lm_losses = lm_losses * M[:, 1:]
            lm_losses = lm_losses.sum(1) / torch.sum(M[:, 1:], 1)
            self.summary_writer.add_scalar('{}/lm_loss'.format(split), lm_losses.sum().item() / X.size(0), iteration)
        # Classification loss
        clf_losses = self.clf_criterion(logits.clf_logits, Y)
        self.summary_writer.add_scalar('{}/clf_loss'.format(split), clf_losses.sum().item() / X.size(0), iteration)
        if only_return_losses:
            return (clf_losses, lm_losses) if logits.lm_logits is not None else clf_losses

        if self.lm_coef > 0 and logits.lm_logits is not None:
            train_loss = clf_losses.sum() + self.lm_coef * lm_losses.sum()
        else:
            train_loss = clf_losses.sum()
        train_loss.backward()
        if self.opt is not None:
            self.opt.step()
            self.opt.zero_grad()
        self.summary_writer.add_scalar('{}/loss'.format(split), train_loss.item() / X.size(0), iteration)
        return train_loss.item()


class ClassificationLossCompute:
    "A Loss compute and train function for classification tasks."

    def __init__(self, lm_criterion, clf_criterion, lm_coef, opt=None):
        self.lm_criterion = lm_criterion
        self.clf_criterion = clf_criterion
        self.lm_coef = lm_coef
        self.opt = opt

    def __call__(self, X, Y, M, logits=None, only_return_losses=False):
        # Language modeling loss
        if logits.lm_logits is not None:
            x_shifted = X[:, 1:, 0].contiguous().view(-1)
            M = M.view(-1, M.size(-1))
            lm_losses = self.lm_criterion(logits.lm_logits, x_shifted)
            lm_losses = lm_losses.view(X.size(0), X.size(-2) - 1)
            lm_losses = lm_losses * M[:, 1:]
            lm_losses = lm_losses.sum(1) / torch.sum(M[:, 1:], 1)
        # Classification loss
        clf_losses = self.clf_criterion(logits.clf_logits, Y)
        if only_return_losses:
            return (clf_losses, lm_losses) if logits.lm_logits is not None else clf_losses

        if self.lm_coef > 0 and logits.lm_logits is not None:
            train_loss = clf_losses.sum() + self.lm_coef * lm_losses.sum()
        else:
            train_loss = clf_losses.sum()
        train_loss.backward()
        if self.opt is not None:
            self.opt.step()
            self.opt.zero_grad()
        return train_loss.item()


# TODO Implement a LossCompute class for similiraty tasks.


class GenerationLossCompute:
    "A Loss compute and train function for multiple choice tasks."

    def __init__(self, lm_criterion, clf_criterion, lm_coef, opt=None, generate_start_step=0, summary_writer=None):
        self.summary_writer = summary_writer
        self.lm_criterion = lm_criterion
        self.clf_criterion = clf_criterion
        self.lm_coef = lm_coef
        self.opt = opt
        self.generate_start_step = generate_start_step

    def lm_loss(self, X, M, lm_logits, split, iteration):
        x_shifted = X[:, :, 1:, 0].contiguous().view(-1)  # Shape: 252
        M = M.view(-1, M.size(2))
        lm_losses = self.lm_criterion(lm_logits, x_shifted)
        lm_losses = lm_losses.view(X.size(0) * X.size(1), X.size(2) - 1)
        lm_losses = lm_losses * M[:, 1:]
        lm_losses = lm_losses.sum(1) / torch.sum(M[:, 1:], 1)
        self.summary_writer.add_scalar('{}/lm_loss'.format(split), lm_losses.sum().item() / X.size(0), iteration)
        return lm_losses

    def gen_loss(self, G, gen_logits, split, iteration):
        A, MA = G
        max_a_len = int(gen_logits.size(0) / MA.size(0))
        a_shifted = A[:, :, :max_a_len, 0].contiguous().view(-1)
        MA = MA.view(-1, MA.size(2))
        gen_losses = self.lm_criterion(gen_logits, a_shifted)
        gen_losses = gen_losses.view(A.size(0) * A.size(1), max_a_len)
        gen_losses = gen_losses * MA[:, :max_a_len]
        gen_losses = gen_losses.sum(1) / torch.sum(MA, 1)
        self.summary_writer.add_scalar('{}/gen_loss'.format(split), gen_losses.sum().item() / A.size(0), iteration)
        return gen_losses

    def __call__(self, X, Y, M, G=None,
                 logits=None, only_return_losses=False, iteration=0, split=None):
        generate = iteration > self.generate_start_step
        # Language modeling loss
        if logits.lm_logits is not None:
            lm_losses = self.lm_loss(X, M, logits.lm_logits, split, iteration)
        else:
            lm_losses = 0

        if logits.gen_logits is not None:
            gen_losses = self.gen_loss(G, logits.gen_logits, split, iteration)
        else:
            gen_losses = 0

        # Classification loss
        clf_losses = self.clf_criterion(logits.clf_logits, Y)
        self.summary_writer.add_scalar('{}/clf_loss'.format(split), clf_losses.sum().item() / X.size(0), iteration)
        if only_return_losses:
            return (clf_losses, lm_losses) if logits.lm_logits is not None else clf_losses

        if self.lm_coef > 0 and logits.lm_logits is not None:
            if generate:
                train_loss = clf_losses.sum() + self.lm_coef * lm_losses.sum() + gen_losses.sum()
            else:
                train_loss = clf_losses.sum() + self.lm_coef * lm_losses.sum()
        else:
            if generate:
                train_loss = clf_losses.sum() + gen_losses.sum()
            else:
                train_loss = clf_losses.sum()
        train_loss.backward()
        if self.opt is not None:
            self.opt.step()
            self.opt.zero_grad()
        self.summary_writer.add_scalar('{}/loss'.format(split), train_loss.item() / X.size(0), iteration)
        return train_loss.item()
